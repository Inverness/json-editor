﻿using System;
using System.Windows;
using JsonEditor.Editors;
using JsonEditor.Editors.Complex;
using Prism.Ioc;
using Prism.Modularity;

namespace JsonEditor
{
    [Module]
    public class ComplexEditorsModule : IModule
    {
        public ComplexEditorsModule()
        {
        }

        public void OnInitialized(IContainerProvider containerProvider)
        {
            var lr = new ResourceDictionary
            {
                Source = new Uri("pack://application:,,,/JsonEditor.Complex;component/Library.xaml")
            };

            Application.Current.Resources.MergedDictionaries.Add(lr);
        }

        public void RegisterTypes(IContainerRegistry reg)
        {
            reg.Register<IEditorFactory, ComplexEditorFactory>("Complex");
            reg.Register<IEditorFactory, FilePathEditorFactory>("FilePath");
            reg.Register<FileCache>();
        }
    }
}
