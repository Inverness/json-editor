﻿using System.Windows.Media;
using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Complex
{
    public sealed class ColorEditor : ComponentStringEditor
    {
        private byte _red;
        private byte _green;
        private byte _blue;
        private byte _alpha;
        private Color _color;
        private bool _updating;

        public ColorEditor(EditorJTokenViewModel host)
            : base(host)
        {
            byte[] bytes = GetTokenValues<byte>(4);
            _red = bytes[0];
            _green = bytes[1];
            _blue = bytes[2];
            _alpha = bytes[3];
            _color = new Color { R = _red, G = _green, B = _blue, A = _alpha };
        }

        public byte Red
        {
            get { return _red; }

            set { SetComponent(ref _red, value); }
        }

        public byte Green
        {
            get { return _green; }

            set { SetComponent(ref _green, value); }
        }

        public byte Blue
        {
            get { return _blue; }

            set { SetComponent(ref _blue, value); }
        }

        public byte Alpha
        {
            get { return _alpha; }

            set { SetComponent(ref _alpha, value); }
        }

        public Color Color
        {
            get { return _color; }

            set
            {
                if (SetProperty(ref _color, value))
                    OnBrushUpdated();
            }
        }

        protected override void OnComponentUpdated()
        {
            if (_updating)
                return;
            _updating = true;

            SetTokenValues(_red, _green, _blue, _alpha);
            Color = new Color { R = _red, G = _green, B = _blue, A = _alpha };

            _updating = false;
        }

        private void OnBrushUpdated()
        {
            if (_updating)
                return;
            _updating = true;

            Red = _color.R;
            Green = _color.G;
            Blue = _color.B;
            Alpha = _color.A;
            SetTokenValues(_red, _green, _blue, _alpha);

            _updating = false;
        }
    }
}
