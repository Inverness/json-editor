﻿using System.Windows.Controls;

namespace JsonEditor.Editors.Complex
{
    /// <summary>
    /// Interaction logic for ColorEditorControl.xaml
    /// </summary>
    public partial class ColorEditorControl : UserControl
    {
        public ColorEditorControl()
        {
            InitializeComponent();
        }
    }
}
