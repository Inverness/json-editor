﻿using System;
using System.Collections.Generic;

namespace JsonEditor.Editors.Complex
{
    public class ComplexEditorFactory : EditorFactoryBase
    {
        private static readonly Dictionary<string, Type> s_types = new Dictionary<string, Type>
        {
            [nameof(ColorEditor)] = typeof(ColorEditor),
            [nameof(Vector2Editor)] = typeof(Vector2Editor),
            [nameof(SizeEditor)] = typeof(SizeEditor),
            [nameof(RectangleEditor)] = typeof(RectangleEditor)
        };

        protected override IDictionary<string, Type> Types => s_types;
    }
}