﻿using System.Runtime.CompilerServices;
using JsonEditor.Editors.Core;
using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Complex
{
    /// <summary>
    ///     A helper base class for values stored in comma-delimited strings.
    /// </summary>
    public abstract class ComponentStringEditor : StringEditor
    {
        protected ComponentStringEditor(EditorJTokenViewModel host) : base(host)
        {
        }

        /// <summary>
        /// Invoked when a component field is updated using SetComponent().
        /// </summary>
        protected virtual void OnComponentUpdated()
        {
            
        }

        /// <summary>
        /// Parses the current StringValue for values using the type converter of the specified type.
        /// </summary>
        /// <typeparam name="T">The type to be parsed.</typeparam>
        /// <param name="expected">The expected number of elements in the string.</param>
        /// <returns>An array containing parsed values.</returns>
        protected T[] GetTokenValues<T>(uint expected)
        {
            string stringValue = StringValue;

            return !string.IsNullOrEmpty(stringValue)
                       ? StringUtility.ConvertValuesFromString<T>(stringValue, expected)
                       : new T[expected];
        }

        /// <summary>
        /// Sets a new StringValue by converting the specified array of values to a comma-delimited string.
        /// </summary>
        /// <typeparam name="T">The array element type.</typeparam>
        /// <param name="values">The array of values to be converted to a string.</param>
        protected void SetTokenValues<T>(params T[] values)
        {
            StringValue = values != null ? StringUtility.ConvertValuesToString(values) : null;
        }

        /// <summary>
        /// A helper method that wraps SetProperty(). Invokes OnComponentUpdated() if there is a change.
        /// </summary>
        /// <typeparam name="T">The value type.</typeparam>
        /// <param name="field">The field to be updated.</param>
        /// <param name="value">The new value.</param>
        /// <param name="propertyName">The name of the property that wraps the field.</param>
        /// <returns>True if the value changed.</returns>
        protected bool SetComponent<T>(ref T field, T value, [CallerMemberName] string propertyName = null)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            if (!SetProperty(ref field, value, propertyName))
                return false;
            OnComponentUpdated();
            return true;
        }
    }
}
