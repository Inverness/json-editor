using System.Collections.ObjectModel;
using JsonEditor.Infrastructure;

namespace JsonEditor.Editors.Complex
{
    public class FileCache
    {
        private readonly IConfigService _configService;

        public FileCache(IConfigService configService)
        {
            _configService = configService;
            Files = new ObservableCollection<string>(new[] {"test", "123", "455"});
        }

        public ObservableCollection<string> Files { get; } 
    }
}