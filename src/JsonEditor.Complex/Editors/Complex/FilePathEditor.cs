﻿using System.ComponentModel;
using System.IO;
using System.Windows.Input;
using System.Windows.Media;
using JsonEditor.Editors.Core;
using JsonEditor.Model;
using JsonEditor.ViewModels;
using Ookii.Dialogs.Wpf;
using Prism.Commands;

namespace JsonEditor.Editors.Complex
{
    /// <summary>
    /// Provides a text box and browse button for editing a file path.
    /// </summary>
    public class FilePathEditor : StringEditor
    {
        private ImageSource _previewImage;
        private readonly bool _relative;
        private readonly bool _forwardSlash;
        private readonly bool _preview;

        public FilePathEditor(EditorJTokenViewModel host)
            : base(host)
        {
            _relative = (bool?) Host.Schema.GetExtensionData(SchemaExtensionNames.Relative) ?? false;
            _forwardSlash = (bool?) Host.Schema.GetExtensionData(SchemaExtensionNames.ForwardSlash) ?? false;
            _preview = (bool?) Host.Schema.GetExtensionData(SchemaExtensionNames.Preview) ?? false;

            BrowseCommand = new DelegateCommand(ExecuteBrowse);
            
            PropertyChanged += OnPropertyChanged;

            UpdatePreviewImage();
        }

        public ICommand BrowseCommand { get; }

        public ImageSource PreviewImage
        {
            get { return _previewImage; }

            set { SetProperty(ref _previewImage, value); }
        }

        public bool PreviewEnabled => _preview;

        private void ExecuteBrowse()
        {
            var dlg = new VistaOpenFileDialog
            {
                Multiselect = false,
                InitialDirectory = Directory.GetCurrentDirectory()
            };

            if (!dlg.ShowDialog().GetValueOrDefault())
                return;

            string newFileName = dlg.FileName;

            if (_relative)
            {
                string docDir = GetDocumentDirectory();
                if (docDir != null)
                    newFileName = FileUtility.MakeRelativePath(docDir, true, newFileName, false) ?? newFileName;
            }

            if (_forwardSlash)
                newFileName = FileUtility.WithForwardDirectorySeparators(newFileName);

            StringValue = newFileName;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(StringValue))
            {
                UpdatePreviewImage();
            }
        }

        private void UpdatePreviewImage()
        {
            if (!_preview)
                return;

            string fullPath = Path.Combine(GetDocumentDirectory(), StringValue);

            PreviewImage = FileUtility.GetThumbnail(fullPath);
        }

        private string GetDocumentDirectory()
        {
            return Path.GetDirectoryName(Host.Document.Path);
        }
    }
}
