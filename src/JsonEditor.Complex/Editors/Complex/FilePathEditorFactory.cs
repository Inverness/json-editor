using JsonEditor.Model;
using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Complex
{
    public class FilePathEditorFactory : IEditorFactory
    {
        private readonly FileCache _fileCache;

        public FilePathEditorFactory(FileCache fileCache)
        {
            _fileCache = fileCache;
        }

        public IEditor TryCreateEditor(EditorJTokenViewModel host)
        {
            return host.Schema.GetExtensionEditorType() == nameof(FilePathEditor) ? new FilePathEditor(host) : null;
        }
    }
}