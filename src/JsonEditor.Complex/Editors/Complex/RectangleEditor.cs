﻿using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Complex
{
    public class RectangleEditor : ComponentStringEditor
    {
        private int _x;
        private int _y;
        private int _w;
        private int _h;

        public RectangleEditor(EditorJTokenViewModel host)
            : base(host)
        {
            int[] values = GetTokenValues<int>(4);
            _x = values[0];
            _y = values[1];
            _w = values[2];
            _h = values[3];
        }

        public int X
        {
            get { return _x; }

            set { SetComponent(ref _x, value); }
        }

        public int Y
        {
            get { return _y; }

            set { SetComponent(ref _y, value); }
        }

        public int Width
        {
            get { return _w; }

            set { SetComponent(ref _w, value); }
        }

        public int Height
        {
            get { return _h; }

            set { SetComponent(ref _h, value); }
        }

        protected override void OnComponentUpdated()
        {
            SetTokenValues(_x, _y, _w, _h);
        }
    }
}
