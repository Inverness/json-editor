﻿using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Complex
{
    public class SizeEditor : ComponentStringEditor
    {
        private float _w;
        private float _h;

        public SizeEditor(EditorJTokenViewModel host)
            : base(host)
        {
            float[] values = GetTokenValues<float>(2);
            _w = values[0];
            _h = values[1];
        }

        public float Width
        {
            get { return _w; }

            set { SetComponent(ref _w, value); }
        }

        public float Height
        {
            get { return _h; }

            set { SetComponent(ref _h, value); }
        }

        protected override void OnComponentUpdated()
        {
            SetTokenValues(_w, _h);
        }
    }
}
