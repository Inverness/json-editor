﻿using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Complex
{
    public class Vector2Editor : ComponentStringEditor
    {
        private float _x;
        private float _y;

        public Vector2Editor(EditorJTokenViewModel host)
            : base(host)
        {
            float[] singles = GetTokenValues<float>(2);
            _x = singles[0];
            _y = singles[1];
        }

        public float X
        {
            get { return _x; }

            set { SetComponent(ref _x, value); }
        }

        public float Y
        {
            get { return _y; }

            set { SetComponent(ref _y, value); }
        }

        protected override void OnComponentUpdated()
        {
            SetTokenValues(_x, _y);
        }
    }
}
