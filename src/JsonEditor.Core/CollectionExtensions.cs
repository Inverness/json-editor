﻿using System;
using System.Collections.Generic;

namespace JsonEditor
{
    public static class CollectionExtensions
    {
        public static TValue GetValueOrDefault<TKey, TValue>(
            this IDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue defaultValue = default(TValue))
        {
            if (dictionary == null)
                throw new ArgumentNullException(nameof(dictionary));
            TValue result;
            return dictionary.TryGetValue(key, out result) ? result : defaultValue;
        }

        public static TValue GetValueOrDefaultReadOnly<TKey, TValue>(
            this IReadOnlyDictionary<TKey, TValue> dictionary,
            TKey key,
            TValue defaultValue = default(TValue))
        {
            if (dictionary == null)
                throw new ArgumentNullException(nameof(dictionary));
            TValue result;
            return dictionary.TryGetValue(key, out result) ? result : defaultValue;
        }
    }
}
