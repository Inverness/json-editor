﻿
namespace JsonEditor
{
    public static class CollectionUtility<T>
    {
        private static T[] s_emptyArray;

        public static T[] EmptyArray => s_emptyArray ?? (s_emptyArray = new T[0]);
    }
}
