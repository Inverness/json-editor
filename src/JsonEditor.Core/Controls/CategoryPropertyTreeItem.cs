﻿
using System.Windows;

namespace JsonEditor.Controls
{
    public class CategoryPropertyTreeItem : PropertyTreeItem
    {
        static CategoryPropertyTreeItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CategoryPropertyTreeItem), new FrameworkPropertyMetadata(typeof(CategoryPropertyTreeItem)));
        }
    }
}
