﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Threading;

namespace JsonEditor.Controls
{
    [StyleTypedProperty(Property = "ItemContainerStyle", StyleTargetType = typeof(PropertyTreeItem))]
    public class PropertyTree : ItemsControl
    {
        /// <summary>
        ///     DependencyProperty for ItemContainerTemplateSelector
        /// </summary>
        public static readonly DependencyProperty ItemContainerTemplateSelectorProperty =
            DependencyProperty.Register("ItemContainerTemplateSelector",
                                        typeof(ItemContainerTemplateSelector),
                                        typeof(PropertyTree));

        private object _currentItem;

        static PropertyTree()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyTree), new FrameworkPropertyMetadata(typeof(PropertyTree)));
        }

        /// <summary>
        ///     Gets or sets the template selector that provides the container for each property tree item. The
        ///     provided container must be a PropertyTreeItem.
        /// </summary>
        public ItemContainerTemplateSelector ItemContainerTemplateSelector
        {
            get { return (ItemContainerTemplateSelector) GetValue(ItemContainerTemplateSelectorProperty); }

            set { SetValue(ItemContainerTemplateSelectorProperty, value); }
        }

        public IEnumerable<PropertyTreeItem> PropertyTreeItems
        {
            get
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    PropertyTreeItem item = GetContainer(i);
                    if (item != null)
                        yield return item;
                }
            }
        }

        /// <summary>
        ///     Updates all item splitter positions to that of the most nested splitter.
        /// </summary>
        public void FitItemSplitters()
        {
            PropertyTreeItem selectedItem = null;
            double selectedWidth = 0;

            foreach (PropertyTreeItem item in PropertyTreeItems)
            {
                if (item.Template == null)
                    continue;

                if (item.NameColumnDefinition == null || item.IndentColumnDefinition == null)
                    item.ApplyTemplate();
                if (item.NameColumnDefinition == null || item.IndentColumnDefinition == null)
                    continue;

                double width = item.IndentColumnDefinition.Width.Value + item.NameColumnDefinition.Width.Value;
                if (width > selectedWidth)
                {
                    selectedItem = item;
                    selectedWidth = width;
                }
            }

            if (selectedItem != null)
                UpdateItemSplitters(selectedItem, selectedWidth);
        }

        // Updates all item splitters, optionally excluding a source. This is called whenever the user drags one of
        // the splitters to keep them in sync.
        internal void UpdateItemSplitters(PropertyTreeItem source, double totalWidth)
        {
            foreach (PropertyTreeItem item in PropertyTreeItems)
            {
                if ((source != null && ReferenceEquals(item, source)) || item.Template == null)
                    continue;

                if (item.NameColumnDefinition == null || item.IndentColumnDefinition == null)
                    item.ApplyTemplate();
                if (item.NameColumnDefinition == null || item.IndentColumnDefinition == null)
                    continue;

                double newWidth = totalWidth - item.IndentColumnDefinition.Width.Value;
                item.NameColumnDefinition.Width = new GridLength(newWidth);
            }
        }

        internal PropertyTreeItem GetContainer(int index)
        {
            var item = Items[index] as PropertyTreeItem;
            if (item != null)
                return item;
            item = ItemContainerGenerator.ContainerFromIndex(index) as PropertyTreeItem;
            return item;
        }

        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);
            Dispatcher.InvokeAsync(FitItemSplitters, DispatcherPriority.Background);
        }

        protected override bool IsItemItsOwnContainerOverride(object item)
        {
            bool result = item is PropertyTreeItem;
            if (!result)
                _currentItem = item;
            return result;
        }

        protected override DependencyObject GetContainerForItemOverride()
        {
            object currentItem = _currentItem;
            _currentItem = null;
            
            DataTemplate itemContainerTemplate = ItemContainerTemplateSelector?.SelectTemplate(currentItem, this);
            if (itemContainerTemplate != null)
            {
                var itemContainer = itemContainerTemplate.LoadContent() as PropertyTreeItem;
                if (itemContainer == null)
                    throw new InvalidOperationException("Invalid item container.");
                return itemContainer;
            }

            return new PropertyTreeItem();
        }
    }
}
