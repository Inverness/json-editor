﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace JsonEditor.Controls
{
    [TemplatePart(Name = "PART_Expander", Type = typeof(ButtonBase))]
    [TemplatePart(Name = "PART_IndentColumnDefinition", Type = typeof(ColumnDefinition))]
    [TemplatePart(Name = "PART_NameColumnDefinition", Type = typeof(ColumnDefinition))]
    [TemplatePart(Name = "PART_Splitter", Type = typeof(GridSplitter))]
    [TemplatePart(Name = "PART_MenuButton", Type = typeof(Button))]
    public class PropertyTreeItem : HeaderedContentControl
    {
        internal const string ExpanderName = "PART_Expander";
        internal const string IndentColumnDefinitionName = "PART_IndentColumnDefinition";
        internal const string NameColumnDefinitionName = "PART_NameColumnDefinition";
        internal const string SplitterName = "PART_Splitter";
        internal const string MenuButtonName = "PART_MenuButton";

        private static readonly DependencyPropertyKey DepthPropertyKey =
            DependencyProperty.RegisterReadOnly("Depth", typeof (int), typeof (PropertyTreeItem),
                                                new FrameworkPropertyMetadata(0));

        public static readonly DependencyProperty DepthProperty = DepthPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey HasChildItemsPropertyKey =
            DependencyProperty.RegisterReadOnly("HasChildItems", typeof(bool), typeof(PropertyTreeItem),
                                                new FrameworkPropertyMetadata(false));

        public static readonly DependencyProperty HasChildItemsProperty = HasChildItemsPropertyKey.DependencyProperty;

        private static readonly DependencyPropertyKey ExpansionVisibilityPropertyKey =
            DependencyProperty.RegisterReadOnly("ExpansionVisibility", typeof (Visibility), typeof (PropertyTreeItem),
                                                new FrameworkPropertyMetadata(Visibility.Visible));

        public static readonly DependencyProperty ExpansionVisibilityProperty =
            ExpansionVisibilityPropertyKey.DependencyProperty;

        private static readonly DependencyProperty ParentItemProperty =
            DependencyProperty.Register("ParentItem", typeof(PropertyTreeItem), typeof(PropertyTreeItem),
                                        new FrameworkPropertyMetadata(OnParentItemChanged));

        public static readonly DependencyProperty IsExpandedProperty =
            DependencyProperty.Register("IsExpanded", typeof(bool), typeof(PropertyTreeItem),
                                        new FrameworkPropertyMetadata(true, OnIsExpandedChanged));

        public static readonly RoutedEvent ExpandedEvent =
            EventManager.RegisterRoutedEvent("Expanded", RoutingStrategy.Bubble, typeof (RoutedEventHandler),
                                             typeof (PropertyTreeItem));

        public static readonly RoutedEvent CollapsedEvent =
            EventManager.RegisterRoutedEvent("Collapsed", RoutingStrategy.Bubble, typeof (RoutedEventHandler),
                                             typeof (PropertyTreeItem));

        internal ColumnDefinition IndentColumnDefinition;
        internal ColumnDefinition NameColumnDefinition;

        private readonly List<PropertyTreeItem> _childItems = new List<PropertyTreeItem>();
        private ControlTemplate _previousTemplate;

        static PropertyTreeItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(PropertyTreeItem), new FrameworkPropertyMetadata(typeof(PropertyTreeItem)));
        }

        public event RoutedEventHandler Expanded
        {
            add { AddHandler(ExpandedEvent, value); }

            remove { RemoveHandler(ExpandedEvent, value); }
        }

        public event RoutedEventHandler Collapsed
        {
            add { AddHandler(CollapsedEvent, value); }

            remove { RemoveHandler(CollapsedEvent, value); }
        }

        public PropertyTreeItem ParentItem
        {
            get { return (PropertyTreeItem) GetValue(ParentItemProperty); }

            set { SetValue(ParentItemProperty, value); }
        }

        public int Depth
        {
            get { return (int) GetValue(DepthProperty); }

            private set { SetValue(DepthPropertyKey, value); }
        }

        public bool HasChildItems
        {
            get { return (bool) GetValue(HasChildItemsProperty); }

            private set { SetValue(HasChildItemsPropertyKey, value); }
        }

        /// <summary>
        ///     Gets or sets whether this item is expanded.
        /// </summary>
        public bool IsExpanded
        {
            get { return (bool) GetValue(IsExpandedProperty); }

            set { SetValue(IsExpandedProperty, value); }
        }

        /// <summary>
        ///     Gets the visibility of this item based on the expansion state of its ancestor items.
        /// </summary>
        public Visibility ExpansionVisibility
        {
            get { return (Visibility) GetValue(ExpansionVisibilityProperty); }

            private set { SetValue(ExpansionVisibilityPropertyKey, value); }
        }

        /// <summary>
        ///     Gets the property tree this item is in.
        /// </summary>
        public PropertyTree PropertyTree => ItemsControl.ItemsControlFromItemContainer(this) as PropertyTree;

        public int Index
        {
            get
            {
                PropertyTree pt = PropertyTree;
                if (pt == null)
                    return -1;
                return pt.Items.IndexOf(this);
            }
        }

        public IReadOnlyList<PropertyTreeItem> ChildItems => _childItems;

        public override void OnApplyTemplate()
        {
            if (_previousTemplate != null)
            {
                var expander = Template.FindName(ExpanderName, this) as ButtonBase;
                if (expander != null)
                {
                    expander.Click -= OnExpanderClick;
                }

                var menuButton = Template.FindName(MenuButtonName, this) as ButtonBase;
                if (menuButton != null)
                {
                    menuButton.Click -= OnMenuButtonClick;
                }

                var splitter = Template.FindName(SplitterName, this) as GridSplitter;
                if (splitter != null)
                {
                    splitter.DragCompleted -= OnSplitterDragCompleted;
                    splitter.DragDelta -= OnSplitterDragDelta;
                }

                IndentColumnDefinition = null;
                NameColumnDefinition = null;
            }

            if (Template != null)
            {
                var expander = Template.FindName(ExpanderName, this) as ButtonBase;
                if (expander != null)
                {
                    expander.Click += OnExpanderClick;
                }

                var menuButton = Template.FindName(MenuButtonName, this) as ButtonBase;
                if (menuButton != null)
                {
                    menuButton.Click += OnMenuButtonClick;
                }

                var splitter = Template.FindName(SplitterName, this) as GridSplitter;
                if (splitter != null)
                {
                    splitter.DragCompleted += OnSplitterDragCompleted;
                    splitter.DragDelta += OnSplitterDragDelta;
                }

                IndentColumnDefinition = Template.FindName(IndentColumnDefinitionName, this) as ColumnDefinition;
                NameColumnDefinition = Template.FindName(NameColumnDefinitionName, this) as ColumnDefinition;
            }

            _previousTemplate = Template;
            ApplyDepth();
        }

        protected override void OnVisualParentChanged(DependencyObject oldParent)
        {
            base.OnVisualParentChanged(oldParent);

            // Ensure that this item is removed from the parent's list of child items.
            if (PropertyTree == null && ParentItem != null)
                ParentItem = null;
        }

        protected virtual void OnMenuButtonClick(object sender, RoutedEventArgs e)
        {
            if (ContextMenu != null)
            {
                ContextMenu.PlacementTarget = this;
                ContextMenu.IsOpen = true;
                e.Handled = true;
            }
        }

        protected virtual void OnExpanderClick(object sender, RoutedEventArgs e)
        {
            IsExpanded = !IsExpanded;
            e.Handled = true;
        }

        protected virtual void OnExpanded(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        protected virtual void OnCollapsed(RoutedEventArgs e)
        {
            RaiseEvent(e);
        }

        protected virtual void UpdateVisualStateEx(bool transition)
        {
            VisualStates.GoToState(this, transition, IsEnabled ? VisualStates.Normal : VisualStates.Disabled);

            VisualStates.GoToState(this, transition, IsExpanded ? VisualStates.Expanded : VisualStates.Collapsed);
        }

        protected virtual void AddChildItem(PropertyTreeItem child)
        {
            _childItems.Add(child);
            if (_childItems.Count == 1)
                HasChildItems = true;
        }

        protected virtual void RemoveChildItem(PropertyTreeItem child)
        {
            _childItems.Remove(child);
            if (_childItems.Count == 0)
                HasChildItems = false;
        }

        protected void UpdateExpansionVisibility(bool recurse)
        {
            PropertyTreeItem p = ParentItem;
            Visibility newVisibility;

            if (p != null)
            {
                newVisibility = p.IsExpanded && p.ExpansionVisibility == Visibility.Visible
                    ? Visibility.Visible
                    : Visibility.Collapsed;
            }
            else
            {
                newVisibility = Visibility.Visible;
            }

            ExpansionVisibility = newVisibility;

            if (recurse)
            {
                foreach (PropertyTreeItem child in _childItems)
                    child.UpdateExpansionVisibility(true);
            }
        }

        /// <summary>
        ///     Updates the splitter positions of sibling controls to match the current.
        /// </summary>
        protected void UpdateItemSplitters()
        {
            var propertyTree = PropertyTree;
            var column = NameColumnDefinition;

            if (propertyTree != null && column != null)
                propertyTree.UpdateItemSplitters(this, column.Offset + column.Width.Value);
        }

        protected void UpdateDepth()
        {
            int newDepth = 0;

            PropertyTreeItem current = ParentItem;
            while (current != null)
            {
                newDepth++;
                current = current.ParentItem;
            }

            Depth = newDepth;
            ApplyDepth();
        }

        private void ApplyDepth()
        {
            // TODO: Make the indent multiplier a DP
            if (IndentColumnDefinition != null)
                IndentColumnDefinition.Width = new GridLength(Depth * 10);
        }

        private static void OnIsExpandedChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var item = (PropertyTreeItem) obj;
            var expanded = (bool) e.NewValue;

            if (expanded)
            {
                item.OnExpanded(new RoutedEventArgs(ExpandedEvent, item));
            }
            else
            {
                item.OnCollapsed(new RoutedEventArgs(CollapsedEvent, item));
            }

            item.UpdateExpansionVisibility(true);
            item.UpdateVisualStateEx(true);
        }

        private static void OnParentItemChanged(DependencyObject obj, DependencyPropertyChangedEventArgs e)
        {
            var item = (PropertyTreeItem) obj;
            var oldParent = (PropertyTreeItem) e.OldValue;
            var newParent = (PropertyTreeItem) e.NewValue;

            oldParent?.RemoveChildItem(item);
            newParent?.AddChildItem(item);

            item.UpdateDepth();
            item.UpdateExpansionVisibility(true);
        }

        private void OnSplitterDragDelta(object sender, DragDeltaEventArgs e)
        {
            // This might be costly
            UpdateItemSplitters();
        }

        private void OnSplitterDragCompleted(object sender, DragCompletedEventArgs e)
        {
            UpdateItemSplitters();
        }
    }
}
