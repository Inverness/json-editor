﻿using System;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;

namespace JsonEditor.Controls
{
    /// <summary>
    ///     Used to display only the selected item while ensuring the visual trees of all other items are cached.
    /// </summary>
    public class SelectionPresenter : Selector
    {
        public SelectionPresenter()
        {
            ItemContainerGenerator.StatusChanged += OnItemContainerGeneratorStatusChanged;
        }

        protected override void OnItemsChanged(NotifyCollectionChangedEventArgs e)
        {
            base.OnItemsChanged(e);

            // Ignore items collection changes if the 
            if (e.NewItems != null && ItemContainerGenerator.Status != GeneratorStatus.ContainersGenerated)
            {
                foreach (object item in e.NewItems)
                {
                    UIElement container = GetContainer(item);
                    if (container != null && !ReferenceEquals(container, GetContainer(SelectedItem)))
                        container.Visibility = Visibility.Hidden;
                }
            }
        }

        protected override void OnSelectionChanged(SelectionChangedEventArgs e)
        {
            Debug.Assert(e.RemovedItems.Count < 2 && e.AddedItems.Count < 2, "e.RemovedItems.Count < 2 && e.AddedItems.Count < 2");

            base.OnSelectionChanged(e);

            UIElement uiElement;
            if (e.RemovedItems.Count == 1 && (uiElement = GetContainer(e.RemovedItems[0])) != null)
                uiElement.Visibility = Visibility.Hidden;
            if (e.AddedItems.Count == 1 && (uiElement = GetContainer(e.AddedItems[0])) != null)
                uiElement.Visibility = Visibility.Visible;
        }

        private void OnItemContainerGeneratorStatusChanged(object sender, EventArgs eventArgs)
        {
            if (ItemContainerGenerator.Status == GeneratorStatus.ContainersGenerated)
            {
                foreach (object item in ItemContainerGenerator.Items)
                {
                    UIElement container = ItemContainerGenerator.ContainerFromItem(item) as UIElement;
                    if (container != null && !ReferenceEquals(container, GetContainer(SelectedItem)))
                        container.Visibility = Visibility.Hidden;
                }
            }
        }

        private UIElement GetContainer(object item)
        {
            var fe = item as UIElement;
            if (fe != null)
                return fe;

            return ItemContainerGenerator.ContainerFromItem(item) as UIElement;
        }
    }
}
