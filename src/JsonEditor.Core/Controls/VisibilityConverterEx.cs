﻿using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace JsonEditor.Controls
{
    public sealed class VisibilityConverterEx : IValueConverter
    {
        public VisibilityConverterEx()
        {
            FalseVisibility = Visibility.Collapsed;
        }

        public Visibility FalseVisibility { get; set; }

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value is bool)
                return (bool) value ? Visibility.Visible : FalseVisibility;
            return value != null ? Visibility.Visible : FalseVisibility;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return (Visibility) value == Visibility.Visible;
        }
    }
}
