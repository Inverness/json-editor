using System.Windows;
using System.Windows.Controls;

namespace JsonEditor.Controls
{
    internal static class VisualStates
    {
        public const string Normal = "Normal";

        public const string Disabled = "Disabled";

        public const string Expanded = "Expanded";

        public const string Collapsed = "Collapsed";

        public static void GoToState(Control control, bool useTransitions, params string[] stateNames)
        {
            if (stateNames == null)
            {
                return;
            }

            foreach (string name in stateNames)
            {
                if (VisualStateManager.GoToState(control, name, useTransitions))
                {
                    break;
                }
            }
        }
    }
}