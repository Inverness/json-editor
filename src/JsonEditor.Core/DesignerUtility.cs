﻿using System;
using System.ComponentModel;
using System.Windows;

namespace JsonEditor
{
    /// <summary>
    /// Provides utilities for working with code at design time.
    /// </summary>
    public static class DesignerUtility
    {
        /// <summary>
        /// Gets whether the application is currently in design mode.
        /// </summary>
        public static bool IsInDesignMode
            => (bool) DesignerProperties.IsInDesignModeProperty.GetMetadata(typeof(FrameworkElement)).DefaultValue;

        public static void VerifyNotDesignMode()
        {
            if (IsInDesignMode)
                throw new InvalidOperationException("Runtime mode only");
        }

        public static void VerifyNotDesignMode(DependencyObject obj)
        {
            if (DesignerProperties.GetIsInDesignMode(obj))
                throw new InvalidOperationException("Runtime mode only");
        }

        public static void VerifyDesignMode()
        {
            if (!IsInDesignMode)
                throw new InvalidOperationException("Design mode only");
        }

        public static void VerifyDesignMode(DependencyObject obj)
        {
            if (!DesignerProperties.GetIsInDesignMode(obj))
                throw new InvalidOperationException("Design mode only");
        }
    }
}
