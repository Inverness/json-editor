using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Core
{
    public class ArrayEditor : ContainerEditor
    {
        public ArrayEditor(EditorJTokenViewModel host)
            : base(host)
        {
        }

        protected override void ExecuteAdd()
        {
            Host.DocumentViewModel.AddArrayItemToken(Host, null);
            base.ExecuteAdd();
        }
    }
}