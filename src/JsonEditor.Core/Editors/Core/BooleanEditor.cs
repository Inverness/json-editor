using JsonEditor.ViewModels;
using Newtonsoft.Json.Linq;

namespace JsonEditor.Editors.Core
{
    public class BooleanEditor : EditorBase
    {
        public BooleanEditor(EditorJTokenViewModel host)
            : base(host)
        {
        }

        public bool Value
        {
            get { return (bool) ((JValue) Host.Token).Value; }

            set
            {
                ((JValue) Host.Token).Value = value;
                MarkDirty();
            }
        }
    }
}