using System.Collections.Specialized;
using System.Windows.Input;
using JsonEditor.ViewModels;
using Newtonsoft.Json.Linq;
using Prism.Commands;

namespace JsonEditor.Editors.Core
{
    public abstract class ContainerEditor : EditorBase
    {
        private readonly DelegateCommand _deleteAllCommand;
        private readonly DelegateCommand _addCommand;

        protected ContainerEditor(EditorJTokenViewModel host)
            : base(host)
        {
            var collection = (INotifyCollectionChanged) host.DocumentViewModel.Items;
            collection.CollectionChanged += OnDocumentItemsChanged;

            _deleteAllCommand = new DelegateCommand(ExecuteDeleteAll);
            _addCommand = new DelegateCommand(ExecuteAdd);
        }

        public int ChildCount => ((JContainer) Host.Token).Count;

        public bool CanEdit => Host.Parent != null;

        public ICommand DeleteAllCommand => _deleteAllCommand;

        public ICommand AddCommand => _addCommand;

        public override void Dispose()
        {
            var collection = (INotifyCollectionChanged) Host.DocumentViewModel.Items;
            collection.CollectionChanged -= OnDocumentItemsChanged;
            base.Dispose();
        }

        protected virtual void ExecuteDeleteAll()
        {
            Host.Document.RemoveChildTokens(Host.Token, false);

            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged("ChildCount");
        }

        protected virtual void ExecuteAdd()
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged("ChildCount");
        }

        protected virtual void OnDocumentItemsChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            // ReSharper disable once ExplicitCallerInfoArgument
            RaisePropertyChanged("ChildCount");
        }
    }
}