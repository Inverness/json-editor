﻿using System.Collections.ObjectModel;
using System.Diagnostics;
using JsonEditor.ViewModels;
using Newtonsoft.Json.Linq;

namespace JsonEditor.Editors.Core
{
    public class EnumEditor : EditorBase
    {
        private EnumItem _selectedItem;

        public EnumEditor(EditorJTokenViewModel host)
            : base(host)
        {
            Debug.Assert(host.Schema.Enum.Count != 0, "host.Schema.Enum.Count != 0");
            Debug.Assert(host.Token is JValue, "host.Token is JValue");

            Items = new ObservableCollection<EnumItem>();

            foreach (JToken value in host.Schema.Enum)
            {
                Debug.Assert(value is JValue, "value is JValue");

                var item = new EnumItem(value.ToString(), value);
                Items.Add(item);

                if (host.Token.Equals(value))
                    _selectedItem = item;
            }

            if (_selectedItem == null)
                _selectedItem = Items[0];
        }

        public ObservableCollection<EnumItem> Items { get; }

        public EnumItem SelectedItem
        {
            get { return _selectedItem; }

            set
            {
                if (SetProperty(ref _selectedItem, value))
                    OnSelectedItemChanged();
            }
        }

        protected virtual void OnSelectedItemChanged()
        {
            ((JValue) Host.Token).Value = ((JValue) SelectedItem.Value).Value;
            MarkDirty();
        }
    }

    public class EnumItem
    {
        public EnumItem(string name, JToken value)
        {
            Name = name;
            Value = value;
        }

        public string Name { get; }

        public JToken Value { get; }
    }
}
