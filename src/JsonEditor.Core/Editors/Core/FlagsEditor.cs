﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using JsonEditor.Model;
using JsonEditor.ViewModels;
using Newtonsoft.Json.Linq;
using Prism.Mvvm;

namespace JsonEditor.Editors.Core
{
    public class FlagsEditor : EditorBase
    {
        private bool _updating;

        public FlagsEditor(EditorJTokenViewModel host)
            : base(host)
        {
            if (host.Token.Type != JTokenType.Integer)
                throw new InvalidOperationException();

            Items = new ObservableCollection<FlagItem>();

            var value = (ulong) host.Token;

            var flags = (JObject) host.Schema.GetExtensionData(SchemaExtensionNames.Flags);

            foreach (KeyValuePair<string, JToken> flag in flags)
            {
                var flagValue = (ulong) flag.Value;
                Items.Add(new FlagItem(this, flag.Key, flagValue, (value & flagValue) != 0));
            }
        }

        public ObservableCollection<FlagItem> Items { get; }

        public ulong ValueAsUInt64
        {
            get { return (ulong) ((JValue) Host.Token); }

            set
            {
                ((JValue) Host.Token).Value = value;
                MarkDirty();
            }
        }

        internal void OnEnumFlagItemIsCheckedChanged(FlagItem item)
        {
            if (_updating)
                return;
            _updating = true;

            ulong value = ValueAsUInt64;
            if (item.IsChecked)
                value |= item.IntValue;
            else
                value &= ~item.IntValue;
            ValueAsUInt64 = value;

            foreach (FlagItem otherItem in Items)
            {
                if (otherItem != item)
                    otherItem.IsChecked = (value & otherItem.IntValue) == otherItem.IntValue;
            }

            MarkDirty();
            _updating = false;
        }
    }

    public class FlagItem : BindableBase
    {
        private readonly FlagsEditor _editor;
        private bool _isChecked;

        internal FlagItem(FlagsEditor editor, string name, ulong intValue, bool isChecked)
        {
            _editor = editor;
            Name = name;
            IntValue = intValue;
            _isChecked = isChecked;
        }

        public string Name { get; private set; }

        public ulong IntValue { get; }

        public bool IsChecked
        {
            get { return _isChecked; }

            set
            {
                if (SetProperty(ref _isChecked, value))
                    _editor.OnEnumFlagItemIsCheckedChanged(this);
            }
        }

        internal void OnSiblingChanged()
        {
            RaisePropertyChanged("IsChecked");
        }
    }
}
