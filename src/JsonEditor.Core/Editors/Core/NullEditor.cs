﻿using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Core
{
    public class NullEditor : EditorBase
    {
        public NullEditor(EditorJTokenViewModel host)
            : base(host)
        {
        }
    }
}
