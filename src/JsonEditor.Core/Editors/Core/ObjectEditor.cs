using JsonEditor.ViewModels;

namespace JsonEditor.Editors.Core
{
    public class ObjectEditor : ContainerEditor
    {
        public ObjectEditor(EditorJTokenViewModel host)
            : base(host)
        {
        }

        protected override void ExecuteAdd()
        {
            Host.DocumentViewModel.AddObjectPropertyToken(Host, null);
            base.ExecuteAdd();
        }
    }
}