using System;
using System.Diagnostics;
using JsonEditor.ViewModels;
using Newtonsoft.Json.Linq;

namespace JsonEditor.Editors.Core
{
    public class StringEditor : EditorBase
    {
        public StringEditor(EditorJTokenViewModel host)
            : base(host)
        {
            Debug.Assert(host.Token is JValue);
        }

        public string StringValue
        {
            get { return ValueToken.Value?.ToString(); }

            set
            {
                if (TryUpdateValue(ValueToken, value))
                {
                    MarkDirty();
                    RaisePropertyChanged();
                }
            }
        }

        protected JValue ValueToken => (JValue) Host.Token;

        private bool TryUpdateValue(JValue token, string value)
        {
            switch (token.Type)
            {
                case JTokenType.Integer:
                    long longValue;
                    if (!long.TryParse(value, out longValue))
                        return false;
                    token.Value = longValue;
                    break;
                case JTokenType.Float:
                    double doubleValue;
                    if (!double.TryParse(value, out doubleValue))
                        return false;
                    token.Value = doubleValue;
                    break;
                case JTokenType.String:
                    token.Value = value;
                    break;
                case JTokenType.Date:
                    DateTime dateTimeValue;
                    if (!DateTime.TryParse(value, out dateTimeValue))
                        return false;
                    token.Value = dateTimeValue;
                    break;
                case JTokenType.Guid:
                    Guid guidValue;
                    if (!Guid.TryParse(value, out guidValue))
                        return false;
                    token.Value = guidValue;
                    break;
                case JTokenType.Uri:
                    Uri uriValue;
                    if (!Uri.TryCreate(value, UriKind.RelativeOrAbsolute, out uriValue))
                        return false;
                    token.Value = uriValue;
                    break;
                case JTokenType.TimeSpan:
                    TimeSpan timeSpanValue;
                    if (!TimeSpan.TryParse(value, out timeSpanValue))
                        return false;
                    token.Value = timeSpanValue;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return true;
        }
    }
}