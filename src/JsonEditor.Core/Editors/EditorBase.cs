﻿using System;
using JsonEditor.ViewModels;
using Prism.Mvvm;

namespace JsonEditor.Editors
{
    public abstract class EditorBase : BindableBase, IDisposable, IEditor
    {
        protected EditorBase(EditorJTokenViewModel host)
        {
            if (host == null)
                throw new ArgumentNullException(nameof(host));
            Host = host;
        }

        public EditorJTokenViewModel Host { get; }

        public virtual void Dispose()
        {
            
        }

        protected void MarkDirty()
        {
            Host.DocumentViewModel.IsDirty = true;
        }
    }
}
