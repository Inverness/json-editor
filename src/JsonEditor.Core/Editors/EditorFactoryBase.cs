﻿using System;
using System.Collections.Generic;
using JsonEditor.Model;
using JsonEditor.ViewModels;

namespace JsonEditor.Editors
{
    public abstract class EditorFactoryBase : IEditorFactory
    {
        protected abstract IDictionary<string, Type> Types { get; }

        public virtual IEditor TryCreateEditor(EditorJTokenViewModel host)
        {
            string typeName = host.Schema.GetExtensionEditorType();
            if (typeName == null)
                return null;

            Type type;
            return Types.TryGetValue(typeName, out type) ? (IEditor) Activator.CreateInstance(type, host) : null;
        }
    }
}