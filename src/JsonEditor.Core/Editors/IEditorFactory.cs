﻿using JsonEditor.ViewModels;

namespace JsonEditor.Editors
{
    /// <summary>
    ///     A factory for creating a custom editor.
    /// </summary>
    public interface IEditorFactory
    {
        IEditor TryCreateEditor(EditorJTokenViewModel host);
    }
}
