﻿using System;
using System.IO;
using System.Text;
using System.Windows.Media;
using Microsoft.WindowsAPICodePack.Shell;

namespace JsonEditor
{
    public static class FileUtility
    {
        /// <summary>
        /// Trim a common root from a path. This makes a path relative but without backing out of the directory.
        /// </summary>
        public static string TrimRoot(string root, string path)
        {
            if (root == null)
                throw new ArgumentNullException(nameof(root));

            // Check if path is already rooted
            if (Path.IsPathRooted(path))
                return path;
            
            // Find out where the relative portion begins begin trimming components from the root until there is a match

            int relBegin = -1;
            string actualRoot = Path.GetPathRoot(root);

            while (root.Length > actualRoot.Length)
            {
                if (path.StartsWith(root))
                {
                    relBegin = root.Length;
                    break;
                }

                root = Path.GetDirectoryName(root) ?? actualRoot;
            }

            // Find if we need to skip the directory separator

            bool trimDirSep = relBegin < path.Length &&
                              (path[relBegin] == Path.DirectorySeparatorChar ||
                               path[relBegin] == Path.AltDirectorySeparatorChar);

            if (trimDirSep)
                relBegin++;

            if (relBegin != -1)
                path = path.Substring(relBegin);

            return path;
        }

        /// <summary>
        /// Make a relative path from a directory or file to a directory or file.
        /// </summary>
        /// <param name="fromPath">The from path.</param>
        /// <param name="fromDirectory">Whether the from path is a directory.</param>
        /// <param name="toPath">The to path.</param>
        /// <param name="toDirectory">Whether the to path is a directory.</param>
        /// <returns>A relative path, or null if one could not be made.</returns>
        public static string MakeRelativePath(string fromPath, bool fromDirectory, string toPath, bool toDirectory)
        {
            var rootAttr = fromDirectory ? FileAttributes.Directory : FileAttributes.Normal;
            var pathAttr = toDirectory ? FileAttributes.Directory : FileAttributes.Normal;
            
            var sb = new StringBuilder(260);
            return NativeMethods.PathRelativePathTo(sb, fromPath, rootAttr, toPath, pathAttr) ? sb.ToString() : null;
        }

        /// <summary>
        /// Replaces backslash directory separators with forward slash directory separators.
        /// </summary>
        public static string WithForwardDirectorySeparators(string path)
        {
            return path.Replace(Path.DirectorySeparatorChar, Path.AltDirectorySeparatorChar);
        }

        public static ImageSource GetThumbnail(string path)
        {
            return File.Exists(path) ? ShellFile.FromFilePath(path).Thumbnail?.ExtraLargeBitmapSource : null;
        }
    }
}
