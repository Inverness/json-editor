﻿using System;
using System.Windows;
using System.Windows.Interop;
using static JsonEditor.NativeMethods;

// ReSharper disable InconsistentNaming

namespace JsonEditor
{
    public static class IconUtility
    {
        /// <summary>
        ///     Removes the icon from a window. This should be called from a Window.SourceInitialized event handler.
        /// </summary>
        /// <param name="window"> A window. </param>
        public static void RemoveIcon(Window window)
        {
            if (window == null)
                throw new ArgumentNullException(nameof(window));

            // Get this window's handle
            IntPtr hwnd = new WindowInteropHelper(window).Handle;
            if (hwnd == IntPtr.Zero)
                return;

            // Change the extended window style to not show a window icon
            int extendedStyle = GetWindowLong(hwnd, GWL_EXSTYLE);
            SetWindowLong(hwnd, GWL_EXSTYLE, extendedStyle | WS_EX_DLGMODALFRAME);

            // Update the window's non-client area to reflect the changes
            uint flags = SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER | SWP_FRAMECHANGED | SWP_NOACTIVATE;
            SetWindowPos(hwnd, IntPtr.Zero, 0, 0, 0, 0, flags);
        }
    }
}
