using System;

namespace JsonEditor.Infrastructure
{
    public delegate void BackgroundAction(IProgress<float?> progress);
}