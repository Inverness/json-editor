using System;

namespace JsonEditor.Infrastructure
{
    public delegate T BackgroundFunc<out T>(IProgress<float?> progress);
}