using System;

namespace JsonEditor.Infrastructure
{
    public abstract class BackgroundTask
    {
        protected BackgroundTask(string name, Delegate action)
        {
            Name = name;
            Action = action;
        }

        public string Name { get; }

        public Delegate Action { get; }
    }
}