using System;

namespace JsonEditor.Infrastructure
{
    public class BackgroundTaskEventArgs : EventArgs
    {
        public BackgroundTaskEventArgs(BackgroundTask task, float? progress)
        {
            Task = task;
            Progress = progress;
        }

        public BackgroundTask Task { get; private set; }

        public float? Progress { get; private set; }
    }
}