﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace JsonEditor.Infrastructure
{
    /// <summary>
    /// Handles executing actions on a background thread.
    /// </summary>
    public interface IBackgroundTaskService
    {
        event EventHandler<BackgroundTaskEventArgs> TaskStarted;

        event EventHandler<BackgroundTaskEventArgs> TaskProgress;

        event EventHandler<BackgroundTaskEventArgs> TaskEnded;

        Task Run(string name, BackgroundAction action, CancellationToken ct = default(CancellationToken));

        Task<T> Run<T>(string name, BackgroundFunc<T> func, CancellationToken ct = default(CancellationToken));
    }
}
