﻿using Newtonsoft.Json.Linq;

namespace JsonEditor.Infrastructure
{
    public interface IConfigService
    {
        JObject Application { get; }

        JObject User { get; }
    }
}
