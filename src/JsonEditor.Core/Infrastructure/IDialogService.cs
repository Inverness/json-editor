﻿using System;
using System.Windows;

namespace JsonEditor.Infrastructure
{
    public interface IDialogService
    {
        string[] ShowOpenFileDialog(string filter, bool multiselect);

        string ShowFolderBrowserDialog();

        MessageBoxResult ShowMessageBox(string text, string caption,
                                        MessageBoxButton button = MessageBoxButton.OK,
                                        MessageBoxImage image = MessageBoxImage.None);

        string ShowInputDialog(string text, string caption);

        void HandleError(Action action);

        T HandleError<T>(Func<T> action, T defaultValue = default(T));
    }
}
