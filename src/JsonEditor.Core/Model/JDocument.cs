﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Prism.Mvvm;

namespace JsonEditor.Model
{
    /// <summary>
    ///     Manages the state of a JSON document. All changes to the document that are not contained within a JValue
    ///     must come through here.
    /// </summary>
    public class JDocument : BindableBase
    {
        private bool _strictObjects;

        public event EventHandler<JTokenEventArgs> TokenRemoving;

        public event EventHandler<JTokenEventArgs> TokenAdded;

        /// <summary>
        ///     Gets or sets a flag indicating whether a document was modified.
        /// </summary>
        public bool Modified { get; set; }

        /// <summary>
        ///     Gets or sets the path from where the document was loaded or has been most recently saved to.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        ///     Gets or sets the JSON root.
        /// </summary>
        public JObject Root { get; set; }

        /// <summary>
        ///     Gets or sets the JSON schema root.
        /// </summary>
        public JSchema Schema { get; set; }

        /// <summary>
        ///     Gets or sets the path from where the schema was loaded.
        /// </summary>
        public Uri SchemaUri { get; set; }

        /// <summary>
        ///     Gets or sets a flag enabling strict object mode.
        /// </summary>
        /// <remarks>
        ///     Strict object mode is intended to make editing easier by adding all object properties defined in a
        ///     schema to an object even if that property is not required. It also prevents the adding or removal
        ///     of properties if additionalProperties is not defined in the schema. This avoids requiring the user
        ///     to know the property names of objects ahead of time for editing.
        /// </remarks>
        public bool StrictObjects
        {
            get { return _strictObjects; }

            set
            {
                if (Root != null)
                    throw new InvalidOperationException("cannot change while document is loaded");
                _strictObjects = value;
            }
        }

        /// <summary>
        ///     Returns the schema for the specified token. The result is cached.
        /// </summary>
        /// <param name="token"> A token. </param>
        /// <returns> A schema matching the specified token, or null if the token would not have a schema. </returns>
        public JSchema GetSchema(JToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));
            if (token is JProperty)
                throw new ArgumentOutOfRangeException(nameof(token), "Cannot get a schema for JProperty. Use the property parent or value.");

            JSchema result = null;

            if (token.Parent == null)
            {
                Debug.Assert(token == Root, "token == Root");
                result = Schema;
            }
            else
            {
                // Skip beyond a JProperty to its object.
                JContainer realParent = token.Parent is JProperty ? token.Parent.Parent : token.Parent;

                // Schema lookup depends on whether the parent is an object or an array.
                JSchema parentSchema = GetSchema(realParent);
                if (parentSchema != null)
                {
                    var parentArray = realParent as JArray;
                    var parentObject = realParent as JObject;
                    if (parentArray != null)
                    {
                        result = GetItemSchema(parentSchema, parentArray.IndexOf(token));
                    }
                    else if (parentObject != null)
                    {
                        string propertyName = ((JProperty) token.Parent).Name;

                        result = GetPropertySchema(parentSchema, propertyName);
                    }
                    else
                    {
                        Debug.Fail("Should never get here");
                    }
                }
            }

            return result;
        }

        /// <summary>
        ///     Gets the schema for a property of the object having the specified schema.
        /// </summary>
        /// <param name="schema"> An object schema. </param>
        /// <param name="name"> A property name. </param>
        /// <returns> The property's schema, or null if the property is not allowed based on the schema or strict object behavior. </returns>
        public JSchema GetPropertySchema(JSchema schema, string name)
        {
            if (schema == null)
                throw new ArgumentNullException(nameof(schema));
            if (name == null)
                throw new ArgumentNullException(nameof(name));

            if (schema.Properties.ContainsKey(name))
                return schema.Properties[name];

            if (schema.PatternProperties.Count != 0)
            {
                foreach (KeyValuePair<string, JSchema> pattern in schema.PatternProperties)
                {
                    if (Regex.IsMatch(name, pattern.Key))
                        return pattern.Value;
                }
            }

            if (!schema.AllowAdditionalProperties)
                return null;

            if (schema.AdditionalProperties != null)
                return schema.AdditionalProperties;

            if (StrictObjects)
                return null; // no custom properties allowed for strict mode

            return new JSchema();
        }

        /// <summary>
        ///     Gets the schema for an item of the array having the specified schema.
        /// </summary>
        /// <param name="schema"> An array schema. </param>
        /// <param name="index"> An item index. </param>
        /// <returns> The item's schema, or null if the item is not allowed based on the schema. </returns>
        public JSchema GetItemSchema(JSchema schema, int index)
        {
            if (schema == null)
                throw new ArgumentNullException(nameof(schema));
            if (index < 0)
                throw new ArgumentOutOfRangeException(nameof(index));

            // ItemsPostionValidation indicates whether an object or array was specified for items in the schema.
            // If an array it will be true and we need to match to the index. If false, then Items will only contain
            // a single schema that will apply to all items.
            if (schema.ItemsPositionValidation)
            {
                if (index < schema.Items.Count)
                    return schema.Items[index];

                if (!schema.AllowAdditionalItems)
                    return null;

                return schema.AdditionalItems ?? new JSchema();
            }

            if (schema.Items.Count != 0)
            {
                Debug.Assert(schema.Items.Count == 1, "schema.Items.Count == 1");
                Debug.Assert(schema.Items[0] != null);
                return schema.Items[0];
            }

            return new JSchema();
        }

        /// <summary>
        ///     Generates a JSON document based on its schema. This initializes tokens to default values or minimum
        ///     values depending on the schema. Object properties are only created if they are specifically defined,
        ///     and array items are only created if there is a schema for the specific position in the array.
        /// </summary>
        public void GenerateFromSchema()
        {
            if (Schema == null)
                return;
            if (Schema.Type != JSchemaType.Object)
                throw new InvalidOperationException("Expected an object as the root type");

            if (Root != null)
                OnTokenRemoving(Root, null);

            Root = (JObject) GenerateToken(Schema);

            if (Root != null)
                OnTokenAdded(Root, null);
        }

        /// <summary>
        ///     Generates a default token based on he specified schema. The type of the token will be the least complex
        ///     type allowable. The token is not added to the document.
        /// </summary>
        /// <param name="schema">A schema</param>
        /// <returns></returns>
        public JToken GenerateToken(JSchema schema)
        {
            if (schema == null)
                throw new ArgumentNullException(nameof(schema));
            if (!schema.Type.HasValue)
                return JValue.CreateNull();

            JSchemaType type = SchemaUtility.GetSimplestType(schema.Type.Value);

            JToken result;
            switch (type)
            {
                case JSchemaType.String:
                    if (schema.MinimumLength.HasValue && schema.MinimumLength.Value > 0)
                        result = JValue.CreateString(new string('X', (int) schema.MinimumLength.Value));
                    else
                        result = JValue.CreateString(string.Empty); // is null or empty better?
                    break;
                case JSchemaType.Number:
                    double floatMin = schema.Minimum ?? 0.0;
                    if (schema.ExclusiveMinimum)
                        floatMin += Math.Min(1.0, schema.Maximum ?? 1.0);
                    result = new JValue(floatMin);
                    break;
                case JSchemaType.Integer:
                    var intMin = (long) (schema.Minimum ?? 0);
                    if (schema.ExclusiveMinimum)
                        intMin += Math.Min(1, (long) (schema.Maximum ?? 1));
                    result = new JValue(intMin);
                    break;
                case JSchemaType.Boolean:
                    result = new JValue(false);
                    break;
                case JSchemaType.Object:
                    var resultObject = new JObject();

                    // In strict object mode, generate properties for everything that isn't required but has a schema.
                    if (StrictObjects && schema.Properties.Count != 0)
                    {
                        foreach (KeyValuePair<string, JSchema> propertySchema in schema.Properties)
                        {
                            if (resultObject[propertySchema.Key] == null)
                                resultObject.Add(propertySchema.Key, GenerateToken(propertySchema.Value));
                        }
                    }

                    // Normally only generate properties that are required
                    if (schema.Required.Count != 0)
                    {
                        foreach (string propertyName in schema.Required)
                        {
                            if (resultObject[propertyName] != null)
                                continue;
                            JSchema propertySchema = GetPropertySchema(schema, propertyName);
                            if (propertySchema == null)
                                throw new InvalidOperationException("no schema found for required property: " + propertyName);
                            resultObject.Add(propertyName, GenerateToken(propertySchema));
                        }
                    }

                    result = resultObject;

                    break;
                case JSchemaType.Array:
                    var resultArray = new JArray();

                    // Only need to generate items if positional validation is used or there is a minimum number
                    // of items.
                    if (schema.ItemsPositionValidation && schema.Items.Count != 0)
                    {
                        //int index = 0;
                        foreach (JSchema itemSchema in schema.Items)
                        {
                            //string itemPath = String.Format("{0}[{1}]", path, index++);

                            JToken itemToken = GenerateToken(itemSchema);
                            if (itemToken != null)
                                resultArray.Add(itemToken);
                        }
                    }

                    if (schema.MinimumItems.HasValue)
                    {
                        for (int i = resultArray.Count; i < schema.MinimumItems.Value; i++)
                        {
                            JSchema itemSchema = GetItemSchema(schema, i);
                            if (itemSchema == null)
                                throw new InvalidOperationException("no schema found for required item");
                            resultArray.Add(GenerateToken(itemSchema));
                        }
                    }

                    result = resultArray;

                    break;
                case JSchemaType.Null:
                    result = JValue.CreateNull();
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            Debug.Assert(result != null, "result != null");
            return result;
        }

        public void GenerateStrictObjectProperties(bool full)
        {
            if (Root == null || Schema == null)
                throw new InvalidOperationException();

            if (!StrictObjects)
                return;

            GenerateStrictObjectProperties(Root, full);
        }

        public void AddObjectPropertyToken(JObject parent, string propertyName, JToken token, int? index)
        {
            var newProperty = new JProperty(propertyName, token);

            if (!index.HasValue)
            {
                parent.Add(newProperty);
            }
            else
            {
                // Find a property to insert before
                JProperty target = null;
                int i = 0;
                foreach (JProperty p in parent.Properties())
                {
                    if (i == index)
                    {
                        target = p;
                        break;
                    }
                    i++;
                }

                if (target != null)
                {
                    target.AddBeforeSelf(newProperty);
                }
                else
                {
                    parent.Add(newProperty);
                }
            }

            OnTokenAdded(token, index);
        }

        public void AddArrayItemToken(JArray parent, JToken token, int? index)
        {
            if (!index.HasValue)
            {
                parent.Add(token);
            }
            else
            {
                if (index.Value > parent.Count)
                    throw new ArgumentOutOfRangeException(nameof(index));

                parent.Insert(index.Value, token);
            }

            OnTokenAdded(token, index);
        }

        public bool RemoveToken(JToken token, bool force)
        {
            if (!force)
            {
                JContainer parent = token.GetParentContainer();
                JSchema parentSchema = parent != null ? GetSchema(parent) : null;

                if (parentSchema != null)
                {
                    var parentProperty = token.Parent as JProperty;
                    if (parentProperty != null)
                    {
                        if (parentSchema.Required.Contains(parentProperty.Name))
                            return false;
                    }
                    else
                    {
                        if (parentSchema.MinimumItems.HasValue && parent.Count == parentSchema.MinimumItems)
                            return false;
                    }
                }
            }

            OnTokenRemoving(token, null);

            // Must call Remove() on the property if there is one
            JToken removeTarget = token.Parent is JProperty ? token.Parent : token;
            removeTarget.Remove();

            Modified = true;

            return true;
        }

        public void RemoveChildTokens(JToken token, bool force)
        {
            foreach (JToken child in token.Children().ToList())
                RemoveToken(child.ExcludeProperty(), force);
        }

        public void ReplaceToken(JToken token, JToken newToken)
        {
            JContainer parentContainer = token.GetParentContainer();
            int? index = null;

            // Find index of token within the container
            if (parentContainer != null)
            {
                if (parentContainer.Type == JTokenType.Array)
                {
                    index = ((JArray) parentContainer).IndexOf(token);
                }
                else
                {
                    index = 0;
                    foreach (JProperty property in ((JObject) parentContainer).Properties())
                    {
                        if (ReferenceEquals(property.Value, token))
                            break;
                        index++;
                    }
                }
            }

            OnTokenRemoving(token, index);

            token.Replace(newToken);

            OnTokenAdded(newToken, index);
        }

        private void GenerateStrictObjectProperties(JObject obj, bool full)
        {
            JSchema schema = GetSchema(obj);
            if (schema == null || Schema.Properties.Count == 0)
                return;

            if (!schema.AllowAdditionalProperties || schema.Properties.Count != 0)
            {
                // For full generation, properties are removed then reinserted in the order specified in the schema
                // Missing properties are generated.
                if (full)
                {
                    Dictionary<string, JProperty> existing = obj.Properties().ToDictionary(p => p.Name, p => p);
                    obj.RemoveAll();

                    // Shouldn't need to generate required properties since document should already be validated.

                    //foreach (string propertyName in schema.Required)
                    //{
                    //    if (!existing.ContainsKey(propertyName))
                    //    {
                    //        JSchema propertySchema = GetPropertySchema(schema, propertyName);
                    //        JToken propertyValue = GenerateToken(propertySchema);
                    //        existing.Add(propertyName, new JProperty(propertyName, propertyValue));
                    //    }
                    //}

                    foreach (KeyValuePair<string, JSchema> propertySchema in schema.Properties)
                    {
                        JProperty property;
                        if (existing.TryGetValue(propertySchema.Key, out property))
                        {
                            obj.Add(property);
                            existing.Remove(propertySchema.Key);
                        }
                        else if (obj[propertySchema.Key] == null)
                        {
                            obj.Add(propertySchema.Key, GenerateToken(propertySchema.Value));
                            existing.Remove(propertySchema.Key);
                        }
                    }

                    // Additional properties re-inserted at the end.
                    if (schema.AllowAdditionalProperties)
                    {
                        foreach (JProperty property in existing.Values)
                            obj.Add(property);
                    }
                }
                else
                {
                    foreach (KeyValuePair<string, JSchema> propertySchema in schema.Properties)
                    {
                        if (obj[propertySchema.Key] == null)
                            obj.Add(propertySchema.Key, GenerateToken(propertySchema.Value));
                    }
                }
            }

            foreach (JProperty childObjects in obj.Properties().Where(p => p.Value.Type == JTokenType.Object))
            {
                GenerateStrictObjectProperties((JObject) childObjects.Value, full);
            }
        }

        private void OnTokenAdded(JToken token, int? index)
        {
            TokenAdded?.Invoke(this, new JTokenEventArgs(token, index));
        }

        private void OnTokenRemoving(JToken token, int? index)
        {
            TokenRemoving?.Invoke(this, new JTokenEventArgs(token, index));
        }
    }
}
