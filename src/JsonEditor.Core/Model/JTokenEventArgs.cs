using System;
using Newtonsoft.Json.Linq;

namespace JsonEditor.Model
{
    public class JTokenEventArgs : EventArgs
    {
        public JTokenEventArgs(JToken token, int? index)
        {
            Token = token;
            Index = index;
        }

        public JToken Token { get; }

        public int? Index { get; }
    }
}