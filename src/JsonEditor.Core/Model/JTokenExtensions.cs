﻿using System;
using Newtonsoft.Json.Linq;

namespace JsonEditor.Model
{
    public static class JTokenExtensions
    {
        public static JContainer GetParentContainer(this JToken token)
        {
            if (token == null)
                throw new ArgumentNullException(nameof(token));

            var property = token as JProperty;
            if (property != null)
                return property.Parent;

            property = token.Parent as JProperty;
            if (property != null)
                return (JObject) property.Parent;

            return (JArray) token.Parent;
        }

        /// <summary>
        /// If the current token is a JProperty, returns the property value token, otherwise returns the current token.
        /// </summary>
        /// <param name="token"></param>
        /// <returns></returns>
        public static JToken ExcludeProperty(this JToken token)
        {
            var property = token as JProperty;
            return property != null ? property.Value : token;
        }
    }
}
