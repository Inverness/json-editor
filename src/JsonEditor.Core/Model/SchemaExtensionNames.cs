﻿
namespace JsonEditor.Model
{
    /// <summary>
    ///     The names of extension properties for schemas.
    /// </summary>
    public static class SchemaExtensionNames
    {
        // ReSharper disable InconsistentNaming

        /// <summary>
        ///     The name of a boolean property indicating whether to collapse a container to a combo box used to
        ///     select its items.
        /// </summary>
        public const string ComboBox = "jeComboBox";

        /// <summary>
        ///     The name of a string property providing an assembly qualified type name of a value editor to use for
        ///     that value.
        /// </summary>
        public const string Editor = "jeEditor";

        /// <summary>
        ///     The name of an integer property indicating how many additional lines to add to a string editor.
        /// </summary>
        public const string Rows = "jeRows";

        /// <summary>
        ///     The name of a boolean property indicating whether the container is expanded by default in the editor.
        /// </summary>
        public const string Expand = "jeExpand";

        /// <summary>
        ///     The name of a boolean property indicating whether the schema will be opened in strict objects mode.
        ///     This property is only valid at the root of a schema.
        /// </summary>
        public const string StrictObjects = "jeStrictObjects";

        public const string IsCategory = "jeIsCategory";

        public const string Flags = "jeFlags";
        
        public const string Relative = "jeRelative";

        public const string ForwardSlash = "jeForwardSlash";

        public const string Preview = "jePreview";

        // ReSharper restore InconsistentNaming
    }
}
