using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace JsonEditor.Model
{
    public static class SchemaExtensions
    {
        public static JToken GetExtensionData(this JSchema schema, string key)
        {
            return schema.ExtensionData.GetValueOrDefault(key);
        }

        public static string GetExtensionEditorType(this JSchema schema)
        {
            return (string) schema.ExtensionData.GetValueOrDefault(SchemaExtensionNames.Editor);
        }

        public static int? GetExtensionRows(this JSchema schema)
        {
            return (int?) schema.ExtensionData.GetValueOrDefault(SchemaExtensionNames.Rows);
        }
    }
}