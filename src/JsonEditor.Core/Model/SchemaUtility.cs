﻿using Newtonsoft.Json.Schema;

namespace JsonEditor.Model
{
    public static class SchemaUtility
    {
        public const JSchemaType AnyType = JSchemaType.Null | JSchemaType.Boolean | JSchemaType.Integer |
                                           JSchemaType.Number | JSchemaType.String | JSchemaType.Array |
                                           JSchemaType.Object;

        public static JSchemaType GetSimplestType(JSchemaType type)
        {
            if ((type & JSchemaType.Null) != 0)
                return JSchemaType.Null;
            if ((type & JSchemaType.Boolean) != 0)
                return JSchemaType.Boolean;
            if ((type & JSchemaType.Integer) != 0)
                return JSchemaType.Integer;
            if ((type & JSchemaType.Number) != 0)
                return JSchemaType.Number;
            if ((type & JSchemaType.String) != 0)
                return JSchemaType.String;
            if ((type & JSchemaType.Array) != 0)
                return JSchemaType.Array;
            if ((type & JSchemaType.Object) != 0)
                return JSchemaType.Object;
            return JSchemaType.None;
        }
    }
}
