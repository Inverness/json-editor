﻿using System;
using System.IO;
using System.Runtime.InteropServices;
using System.Security;
using System.Text;

// ReSharper disable StringLiteralTypo
// ReSharper disable IdentifierTypo
// ReSharper disable InconsistentNaming

namespace JsonEditor
{
    [SuppressUnmanagedCodeSecurity]
    internal static class NativeMethods
    {
        internal const int GWL_EXSTYLE = -20;

        internal const int WS_EX_DLGMODALFRAME = 0x0001;

        internal const uint SWP_NOSIZE = 0x0001;
        internal const uint SWP_NOMOVE = 0x0002;
        internal const uint SWP_NOZORDER = 0x0004;
        internal const uint SWP_NOACTIVATE = 0x0010;
        internal const uint SWP_FRAMECHANGED = 0x0020;

        [DllImport("user32.dll")]
        internal static extern int GetWindowLong(IntPtr hwnd, int index);

        [DllImport("user32.dll")]
        internal static extern int SetWindowLong(IntPtr hwnd, int index, int newStyle);

        [DllImport("user32.dll")]
        internal static extern bool SetWindowPos(
            IntPtr hwnd,
            IntPtr hwndInsertAfter,
            int x,
            int y,
            int width,
            int height,
            uint flags);

        [DllImport("shlwapi.dll", CharSet = CharSet.Auto)]
        internal static extern bool PathRelativePathTo(
            StringBuilder resultPath,
            string fromPath,
            FileAttributes fromAttrs,
            string toPath,
            FileAttributes toAttrs
            );
    }
}
