﻿using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace JsonEditor
{
    public sealed class ReferenceComparer<T> : IEqualityComparer<T>
        where T : class
    {
        public bool Equals(T x, T y)
        {
            return ReferenceEquals(x, y);
        }

        public int GetHashCode(T obj)
        {
            return obj != null ? RuntimeHelpers.GetHashCode(obj) : 0;
        }
    }
}
