﻿using System;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace JsonEditor
{
    public static class Resources
    {
        public static readonly ImageSource FolderIcon16 =
            new BitmapImage(new Uri("pack://application:,,,/JsonEditor.Core;component/Resources/FolderIcon16.png"));
    }
}
