﻿using System;
using System.ComponentModel;
using System.Globalization;
using System.Text.RegularExpressions;

namespace JsonEditor
{
    public static class StringUtility
    {
        private static readonly char[] s_wildcardCharacters = { '*', '?' };

        /// <summary>
        ///		Converts a simple wildcard pattern using * and ? to an equivalent regex pattern.
        /// </summary>
        /// <param name="pattern"> The wildcard pattern. </param>
        /// <returns> A string representing an equivalent regex pattern. </returns>
        public static string WildcardToRegex(string pattern)
        {
            if (pattern == null)
                throw new ArgumentNullException(nameof(pattern));
            return "^" + Regex.Escape(pattern).Replace(@"\*", ".*").Replace(@"\?", ".") + "$";
        }

        /// <summary>
        ///		Check if a string represents a wildcard pattern.
        /// </summary>
        /// <param name="value"> The string to check. </param>
        /// <returns> True if the value is a wildcard pattern. </returns>
        public static bool IsWildcardPattern(string value)
        {
            if (value == null)
                throw new ArgumentNullException(nameof(value));
            return value.IndexOfAny(s_wildcardCharacters) != -1;
        }

        public static T[] ConvertValuesFromString<T>(string value, uint? expected = null)
        {
            if (string.IsNullOrEmpty(value))
                return null;

            TypeConverter valueConverter = TypeDescriptor.GetConverter(typeof(T));

            string[] parts = value.Split(new[] { CultureInfo.InvariantCulture.TextInfo.ListSeparator }, StringSplitOptions.RemoveEmptyEntries);

            if (expected.HasValue && parts.Length != expected.Value)
                throw new FormatException("Unable to convert values from string'" + value + "', expected " + expected.Value + " values.");

            var results = new T[parts.Length];
            for (int i = 0; i < parts.Length; i++)
                results[i] = (T) valueConverter.ConvertFromString(parts[i].Trim());

            return results;
        }

        public static string ConvertValuesToString<T>(T[] values)
        {
            if (values == null)
                return null;

            TypeConverter valueConverter = TypeDescriptor.GetConverter(typeof(T));

            var strings = new string[values.Length];
            for (int i = 0; i < values.Length; i++)
                strings[i] = valueConverter.ConvertToString(values[i]);

            return string.Join(CultureInfo.InvariantCulture.TextInfo.ListSeparator + " ", strings);
        }
    }
}
