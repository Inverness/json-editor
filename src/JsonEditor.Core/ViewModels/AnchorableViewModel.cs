namespace JsonEditor.ViewModels
{
    public abstract class AnchorableViewModel : DockableViewModel
    {
        private bool _isVisible = true;

        public bool IsVisible
        {
            get { return _isVisible; }

            set { SetProperty(ref _isVisible, value); }
        }
    }
}