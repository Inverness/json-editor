﻿using System.Windows.Media;
using Prism.Mvvm;

namespace JsonEditor.ViewModels
{
    public abstract class DockableViewModel : BindableBase
    {
        private string _title;
        private ImageSource _iconSource;
        private string _contentId;
        private bool _isSelected;
        private bool _isActive;

        public string Title
        {
            get { return _title; }

            set { SetProperty(ref _title, value); }
        }

        public ImageSource IconSource
        {
            get { return _iconSource; }

            set { SetProperty(ref _iconSource, value); }
        }

        public string ContentId
        {
            get { return _contentId; }

            set { SetProperty(ref _contentId, value); }
        }

        public bool IsSelected
        {
            get { return _isSelected; }

            set { SetProperty(ref _isSelected, value); }
        }

        public bool IsActive
        {
            get { return _isActive; }

            set { SetProperty(ref _isActive, value); }
        }
    }
}
