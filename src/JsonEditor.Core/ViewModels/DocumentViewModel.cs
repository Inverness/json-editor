﻿using System.IO;

namespace JsonEditor.ViewModels
{
    public abstract class DocumentViewModel : DockableViewModel
    {
        private bool _isDirty = true;
        private string _filePath;

        public bool IsDirty
        {
            get { return _isDirty; }

            set { SetProperty(ref _isDirty, value); }
        }

        public string FilePath
        {
            get { return _filePath; }

            set
            {
                if (SetProperty(ref _filePath, value))
                    RaisePropertyChanged("FileName");
            }
        }

        public string FileName => Path.GetFileName(_filePath);
    }
}