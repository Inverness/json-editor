using System;
using System.Linq;
using JsonEditor.Editors;
using JsonEditor.Editors.Core;
using JsonEditor.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace JsonEditor.ViewModels
{
    /// <summary>
    ///     The view model for a JToken that has an editor.
    /// </summary>
    public class EditorJTokenViewModel : JTokenViewModel
    {
        private IEditor _editor;

        public EditorJTokenViewModel(JDocumentViewModel documentViewModel, JTokenViewModel parent)
            : base(documentViewModel, parent)
        {
        }

        /// <summary>
        ///     Gets or sets the current editor for this token. This object will be displayed in the editing area
        ///     of the property tree.
        /// </summary>
        public IEditor Editor
        {
            get { return _editor; }

            private set
            {
                var oldDisposableEditor = _editor as IDisposable;
                if (SetProperty(ref _editor, value))
                    oldDisposableEditor?.Dispose();
            }
        }

        public override void SetToken(JToken token, JSchema schema, string name)
        {
            base.SetToken(token, schema, name);
            Editor = CreateEditor();
        }

        /// <summary>
        ///     Creates an editor for the current token and schema.
        /// </summary>
        /// <returns></returns>
        protected virtual IEditor CreateEditor()
        {
            if (Token == null)
            {
                return null;
            }

            // Try finding a custom editor creator.
            foreach (IEditorFactory f in DocumentViewModel.EditorFactories)
            {
                IEditor editor = f.TryCreateEditor(this);
                if (editor != null)
                    return editor;
            }

            // Special case enum editing with a combo box. Only value types are supported for this right now.
            if (Schema.Enum.Count != 0 && Token is JValue && Schema.Enum.All(e => e is JValue))
                return new EnumEditor(this);

            var flags = Schema.GetExtensionData(SchemaExtensionNames.Flags) as JObject;
            if (flags != null)
                return new FlagsEditor(this);

            // Create one of the basic editors.
            switch (Token.Type)
            {
                case JTokenType.Object:
                    return new ObjectEditor(this);
                case JTokenType.Array:
                    return new ArrayEditor(this);
                case JTokenType.Integer:
                case JTokenType.Float:
                case JTokenType.String:
                case JTokenType.Date:
                case JTokenType.Guid:
                case JTokenType.Uri:
                case JTokenType.TimeSpan:
                    return new StringEditor(this);
                case JTokenType.Boolean:
                    return new BooleanEditor(this);
                case JTokenType.Null:
                    return new NullEditor(this);
                default:
                    return null;
            }
        }
    }
}