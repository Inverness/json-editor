using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using JsonEditor.Editors;
using JsonEditor.Infrastructure;
using JsonEditor.Model;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace JsonEditor.ViewModels
{
    /// <summary>
    ///     The view model for a JDocument. All JDocument changes must come through here.
    /// </summary>
    public sealed class JDocumentViewModel : DocumentViewModel
    {
        private JDocument _document;

        private readonly ObservableCollection<JTokenViewModel> _items = new ObservableCollection<JTokenViewModel>();

        private readonly IDialogService _dialogService;
        private readonly IEditorFactory[] _editorFactories; 

        public JDocumentViewModel(IDialogService dialogService, IEnumerable<IEditorFactory> factories)
        {
            _editorFactories = factories.ToArray();
            _dialogService = dialogService;
            Items = new ReadOnlyObservableCollection<JTokenViewModel>(_items);

            PropertyChanged += OnPropertyChanged;
        }

        public IReadOnlyCollection<IEditorFactory> EditorFactories => _editorFactories;

        /// <summary>
        ///     Gets the view models of the items in document order.
        /// </summary>
        public ReadOnlyObservableCollection<JTokenViewModel> Items { get; private set; }

        /// <summary>
        ///     Gets the underlying JDocument this view model represents.
        /// </summary>
        public JDocument Document => _document;

        /// <summary>
        /// Initialize the view model with a document.
        /// </summary>
        /// <param name="document">A document.</param>
        public void Initialize(JDocument document)
        {
            if (document == null)
                throw new ArgumentNullException(nameof(document));
            if (_document != null)
                throw new InvalidOperationException("document already initialized");

            _document = document;
            _document.TokenAdded += OnDocumentTokenAdded;
            _document.TokenRemoving += OnDocumentTokenRemoving;

            if (_document.Path == null)
            {
                Title = "New Document";
                IsDirty = true;
            }
            else
            {
                Title = Path.GetFileName(_document.Path);
                IsDirty = false;
            }

            AddTokenViewModel(null, _document.Root, null, true);
        }

        /// <summary>
        ///     Add a new object property token to the specified parent if its valid in the schema. Prompts the user
        ///     to input the property name.
        /// </summary>
        /// <param name="parent"> The parent view model. </param>
        /// <param name="index"> The index to insert the property at, or null to insert at the end. </param>
        public void AddObjectPropertyToken(JTokenViewModel parent, int? index)
        {
            if (_document == null)
                throw new InvalidOperationException("document not initialized");

            var jObject = (JObject) parent.Token;

            string name;
            JSchema schema;
            if (!GetNewPropertyInfo(parent, out name, out schema))
                return;

            JToken newToken = Document.GenerateToken(schema);
            Document.AddObjectPropertyToken(jObject, name, newToken, index);

            IsDirty = true;
        }

        /// <summary>
        ///     Add a new array item token to the specified parent if its valid in the schema.
        /// </summary>
        /// <param name="parent"> The parent view model. </param>
        /// <param name="index"> The index to insert the item at, or null to insert at the end. </param>
        public void AddArrayItemToken(JTokenViewModel parent, int? index)
        {
            var array = (JArray) parent.Token;

            JSchema newSchema = Document.GetItemSchema(parent.Schema, index ?? array.Count);
            if (newSchema == null)
            {
                MessageBox.Show("No schema found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return;
            }

            JToken newToken = Document.GenerateToken(newSchema);
            Document.AddArrayItemToken(array, newToken, index);

            IsDirty = true;
        }

        /// <summary>
        /// Query the user to input a name for the new property, and get a matching schema for the property name.
        /// Notifies the user if there is a property name collision or if a schema is not ofund.
        /// </summary>
        /// <param name="parent">The parent token view model.</param>
        /// <param name="name">The new property name.</param>
        /// <param name="schema">The new property schema.</param>
        /// <returns>True if the new property name and schema are valid.</returns>
        private bool GetNewPropertyInfo(JTokenViewModel parent, out string name, out JSchema schema)
        {
            schema = null;
            name = _dialogService.ShowInputDialog("Input property name:", "New Property");
            if (string.IsNullOrWhiteSpace(name))
            {
                name = null;
                return false;
            }

            if (parent.Token[name] != null)
            {
                MessageBox.Show("Property name conflict", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                name = null;
                return false;
            }

            schema = Document.GetPropertySchema(parent.Schema, name);
            if (schema == null)
            {
                MessageBox.Show("No schema found", "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                name = null;
                return false;
            }

            return true;
        }

        private void OnPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(IsDirty))
                _document.Modified = IsDirty;
        }

        /// <summary>
        /// Creates a new view model for a token as the child of the specified parent.
        /// </summary>
        /// <param name="parent"> The parent view model. </param>
        /// <param name="token"> The token to be added. </param>
        /// <param name="localIndex"> The index within the parent's children to insert at. </param>
        /// <param name="initial"> Optimization when initially creating view models for a document. </param>
        private void AddTokenViewModel(JTokenViewModel parent, JToken token, int? localIndex, bool initial)
        {
            Tuple<JSchema, string> schemaAndName = GetSchemaAndName(token);

            var isCategory = (bool?) schemaAndName.Item1.GetExtensionData(SchemaExtensionNames.IsCategory);

            JTokenViewModel vm;
            if (isCategory.GetValueOrDefault())
                vm = new CategoryJTokenViewModel(this, parent);
            else
                vm = new EditorJTokenViewModel(this, parent);
            vm.SetToken(token, schemaAndName.Item1, schemaAndName.Item2);

            int insertionIndex;
            if (localIndex.HasValue)
            {
                if (localIndex.Value > parent.Children.Count)
                    throw new ArgumentOutOfRangeException(nameof(localIndex));
                insertionIndex = _items.IndexOf(parent) + 1 + localIndex.Value;
            }
            else
            {
                insertionIndex = initial ? _items.Count : FindInsertionIndex(parent);
            }

            parent?.AddChild(vm, localIndex);

            _items.Insert(insertionIndex, vm);

            //if (parent != null && !parent.IsExpanded && parent.Children.Count == 1)
            //    parent.IsExpanded = true;

            AddTokenViewModelChildren(vm, token, initial);
        }

        private void AddTokenViewModelChildren(JTokenViewModel parentVm, JToken parentToken, bool initial)
        {
            if (parentToken.Type == JTokenType.Object)
            {
                foreach (KeyValuePair<string, JToken> item in (JObject) parentToken)
                {
                    AddTokenViewModel(parentVm, item.Value, null, initial);
                }
            }
            else if (parentToken.Type == JTokenType.Array)
            {
                foreach (JToken item in (JArray) parentToken)
                {
                    AddTokenViewModel(parentVm, item, null, initial);
                }
            }
        }

        private void RemoveTokenViewModel(JTokenViewModel vm)
        {
            foreach (JTokenViewModel child in vm.Children)
                RemoveTokenViewModel(child);

            vm.SetToken(null, null, null);
            _items.Remove(vm);

            IsDirty = true;
        }

        private int FindInsertionIndex(JTokenViewModel parent)
        {
            if (parent == null)
                return _items.Count;

            int parentIndex = _items.IndexOf(parent);
            Debug.Assert(parentIndex != -1, "parentIndex != -1");

            if (parent.Children.Count == 0)
                return parentIndex + 1;

            for (int i = parentIndex + 1; i < _items.Count; i++)
            {
                if (_items[i].Depth <= parent.Depth)
                    return i;
            }

            return _items.Count;
        }

        private Tuple<JSchema, string> GetSchemaAndName(JToken token)
        {
            Debug.Assert(token.Root == Document.Root);

            JSchema schema = Document.GetSchema(token) ?? new JSchema();

            string name;
            if (token.Parent == null)
            {
                name = null;
            }
            else
            {
                var property = token.Parent as JProperty;
                name = property != null
                    ? property.Name
                    : ((JArray) token.Parent).IndexOf(token).ToString(CultureInfo.InvariantCulture);
            }

            return Tuple.Create(schema, name);
        }

        private void OnDocumentTokenAdded(object sender, JTokenEventArgs e)
        {
            JContainer parentContainer = e.Token.GetParentContainer();

            JTokenViewModel parentVm = null;
            if (parentContainer != null)
                parentVm = FindTokenViewModel(parentContainer);

            AddTokenViewModel(parentVm, e.Token, e.Index, false);
        }

        private void OnDocumentTokenRemoving(object sender, JTokenEventArgs e)
        {
            JTokenViewModel vm = FindTokenViewModel(e.Token);
            Debug.Assert(vm != null, "view model is missing");
            RemoveTokenViewModel(vm);
        }

        /// <summary>
        /// Gets an existing for model for a token.
        /// </summary>
        private JTokenViewModel FindTokenViewModel(JToken token)
        {
            int index;
            return FindTokenViewModel(token, out index);
        }

        /// <summary>
        /// Gets an existing for model for a token.
        /// </summary>
        private JTokenViewModel FindTokenViewModel(JToken token, out int index)
        {
            int i = 0;
            foreach (JTokenViewModel vm in _items)
            {
                if (ReferenceEquals(vm.Token, token))
                {
                    index = i;
                    return vm;
                }
                i++;
            }

            index = -1;
            return null;
        }
    }
}