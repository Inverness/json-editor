using System.Windows.Input;
using Prism.Mvvm;

namespace JsonEditor.ViewModels
{
    public class JTokenCommand : BindableBase
    {
        private string _name;
        private readonly ICommand _command;

        public JTokenCommand(string name, ICommand command)
        {
            _name = name;
            _command = command;
        }

        public string Name
        {
            get { return _name; }

            set { SetProperty(ref _name, value); }
        }

        public ICommand Command => _command;
    }
}