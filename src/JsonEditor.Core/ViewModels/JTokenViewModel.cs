﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using JsonEditor.Model;
using Prism.Commands;
using Prism.Mvvm;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace JsonEditor.ViewModels
{
    /// <summary>
    ///     A view model for a JToken.
    /// </summary>
    public abstract class JTokenViewModel : BindableBase
    {
        private static readonly JTokenViewModel[] s_emptyChildren = new JTokenViewModel[0];

        private readonly JDocumentViewModel _documentViewModel;
        private List<JTokenViewModel> _children;
        private readonly JTokenViewModel _parent;
        private JToken _token;
        private JSchema _schema;
        private string _name;
        private bool _hasMenuCommands;
        private readonly int _depth;
        private bool _isExpanded = true;

        internal JTokenViewModel(JDocumentViewModel documentViewModel, JTokenViewModel parent)
        {
            _documentViewModel = documentViewModel;
            _parent = parent;

            MenuCommands = new ObservableCollection<JTokenCommand>();
            MenuCommands.CollectionChanged += OnMenuCommandsCollectionChanged;

            _depth = parent?.Depth + 1 ?? 0;
        }

        public int Depth => _depth;

        public JDocumentViewModel DocumentViewModel => _documentViewModel;

        public JDocument Document => _documentViewModel.Document;

        public JTokenViewModel Parent => _parent;

        public IReadOnlyList<JTokenViewModel> Children => (IReadOnlyList<JTokenViewModel>) _children ?? s_emptyChildren;

        public bool IsExpanded
        {
            get { return _isExpanded; }

            set { SetProperty(ref _isExpanded, value); }
        }

        public ObservableCollection<JTokenCommand> MenuCommands { get; }

        public bool HasMenuCommands
        {
            get { return _hasMenuCommands; }

            private set { SetProperty(ref _hasMenuCommands, value); }
        }

        public JToken Token
        {
            get { return _token; }

            private set { SetProperty(ref _token, value); }
        }

        public JSchema Schema
        {
            get { return _schema; }

            private set { SetProperty(ref _schema, value); }
        }

        public string Name
        {
            get { return _name; }

            private set { SetProperty(ref _name, value); }
        }

        public int GetChildIndex(JTokenViewModel vm)
        {
            if (vm.Parent != this)
                throw new ArgumentOutOfRangeException(nameof(vm), "not a child");
            return _children?.IndexOf(vm) ?? -1;
        }

        public virtual void SetToken(JToken token, JSchema schema, string name)
        {
            if (token != null && schema == null)
                throw new InvalidOperationException();

            Token = token;
            Schema = schema;
            Name = name;

            if (schema != null)
            {
                bool? expand = (bool?) schema.GetExtensionData(SchemaExtensionNames.Expand);
                if (expand.HasValue)
                    IsExpanded = expand.Value;
            }

            UpdateMenuCommands(false);
        }

        //public static void SetViewModel(JToken token, TokenViewModel vm)
        //{
        //    token.RemoveAnnotations<TokenViewModel>();
        //    token.AddAnnotation(vm);
        //}

        //public static TokenViewModel GetViewModel(JToken token)
        //{
        //    return token.Annotation<TokenViewModel>();
        //}

        internal virtual void AddChild(JTokenViewModel child, int? index)
        {
            if (_children == null)
                _children = new List<JTokenViewModel>();

            if (index.HasValue)
                _children.Insert(index.Value, child);
            else
                _children.Add(child);
        }

        internal virtual void RemoveChild(JTokenViewModel child)
        {
            _children?.Remove(child);
        }

        protected void UpdateMenuCommands(bool required)
        {
            MenuCommands.Clear();

            if (Token == null || Parent == null || Parent.Token == null)
                return;

            // Strict objects mode disallows manipulating of properties unless explicitly allowing additional properties.
            if (Document.StrictObjects && Parent.Token is JObject && Parent.Schema.AdditionalProperties == null)
                return;

            JProperty parentProperty;
            required |= (parentProperty = Parent.Token as JProperty) != null &&
                        Parent.Schema.Required.Contains(parentProperty.Name);

            MenuCommands.Add(new JTokenCommand("Insert", new DelegateCommand(ExecuteInsert)));
            MenuCommands.Add(new JTokenCommand("Duplicate", new DelegateCommand(ExecuteDuplicate)));
            if (Schema == null || !required)
                MenuCommands.Add(new JTokenCommand("Delete", new DelegateCommand(ExecuteDelete)));


            // Add default types
            JSchemaType types = _schema == null ? SchemaUtility.AnyType : (_schema.Type ?? SchemaUtility.AnyType);

            Action<JSchemaType> addOption = t =>
            {
                if ((types & t) != 0)
                    MenuCommands.Add(new JTokenCommand("Set Type: " + t.ToString(), new DelegateCommand(() => OnSetTypeClick(t))));
            };

            addOption(JSchemaType.String);
            addOption(JSchemaType.Number);
            addOption(JSchemaType.Integer);
            addOption(JSchemaType.Boolean);
            addOption(JSchemaType.Object);
            addOption(JSchemaType.Array);
            addOption(JSchemaType.Null);

            // Add extension types eventually
        }

        protected void ExecuteInsert()
        {
            var parentContainer = (JContainer) Parent.Token;
            if (parentContainer.Type == JTokenType.Array)
                DocumentViewModel.AddArrayItemToken(Parent, Parent.GetChildIndex(this));
            else
                DocumentViewModel.AddObjectPropertyToken(Parent, Parent.GetChildIndex(this));
        }

        protected void ExecuteDuplicate()
        {
            
        }

        protected void ExecuteDelete()
        {
            Document.RemoveToken(Token, false);
        }

        private void OnMenuCommandsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            HasMenuCommands = MenuCommands.Count != 0;
        }

        private void OnSetTypeClick(JSchemaType type)
        {
            switch (type)
            {
                case JSchemaType.String:
                    Document.ReplaceToken(Token, new JValue((string) null));
                    break;
                case JSchemaType.Number:
                    Document.ReplaceToken(Token, new JValue(0.0));
                    break;
                case JSchemaType.Integer:
                    Document.ReplaceToken(Token, new JValue(0));
                    break;
                case JSchemaType.Boolean:
                    Document.ReplaceToken(Token, new JValue(false));
                    break;
                case JSchemaType.Object:
                    Document.ReplaceToken(Token, new JObject());
                    break;
                case JSchemaType.Array:
                    Document.ReplaceToken(Token, new JArray());
                    break;
                case JSchemaType.Null:
                    Document.ReplaceToken(Token, new JValue((object) null));
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
    }
}
