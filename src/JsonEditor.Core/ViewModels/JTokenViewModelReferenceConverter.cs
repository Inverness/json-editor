﻿using System;
using System.Globalization;
using System.Windows.Data;
using JsonEditor.Controls;

namespace JsonEditor.ViewModels
{
    /// <summary>
    ///     Resolves a reference to the PropertyTreeItem of a PropertyTreeItemViewModel.
    ///     This is needed so that view models can reference each other and establish a parent-child relationship.
    /// </summary>
    public class JTokenViewModelReferenceConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            if (values == null)
                throw new ArgumentNullException(nameof(values));
            if (targetType == null)
                throw new ArgumentNullException(nameof(targetType));
            if (values.Length < 2)
                throw new ArgumentException("expected two values", nameof(values));
            if (!typeof(PropertyTreeItem).IsAssignableFrom(targetType))
                throw new ArgumentException("target type must be a PropertyTreeItem");

            var propertyTree = values[0] as PropertyTree;
            if (propertyTree == null)
                throw new ArgumentException("the first value must be a PropertyTree", nameof(values));

            if (values[1] == null)
                return null;

            var viewModel = values[1] as JTokenViewModel;
            if (viewModel == null)
                throw new ArgumentException("the second value must be a PropertyTreeItemViewModel", nameof(values));

            var propertyTreeItem = propertyTree.ItemContainerGenerator.ContainerFromItem(viewModel) as PropertyTreeItem;
            if (propertyTreeItem == null)
                throw new InvalidOperationException("Item container not found");

            return propertyTreeItem;
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
