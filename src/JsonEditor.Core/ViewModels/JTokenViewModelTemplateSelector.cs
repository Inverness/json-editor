﻿using System.Windows;
using System.Windows.Controls;

namespace JsonEditor.ViewModels
{
    public sealed class JTokenViewModelTemplateSelector : ItemContainerTemplateSelector
    {
        public DataTemplate CategoryJTokenTemplate { get; set; }

        public DataTemplate EditorJTokenTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, ItemsControl parentItemsControl)
        {
            if (item is EditorJTokenViewModel)
                return EditorJTokenTemplate;
            if (item is CategoryJTokenViewModel)
                return CategoryJTokenTemplate;
            return null;
        }
    }
}
