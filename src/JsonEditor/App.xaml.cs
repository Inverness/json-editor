﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using JsonEditor.Infrastructure;
using JsonEditor.ViewModels;
using JsonEditor.Views;
using Prism.Ioc;
using Prism.Modularity;
using Prism.Unity;
using Unity;

namespace JsonEditor
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : PrismApplication
    {
        private ConfigService _configService;

        static App()
        {
            AppDomain.CurrentDomain.AssemblyResolve += OnAssemblyResolve;
        }

        protected override void OnStartup(StartupEventArgs e)
        {
            base.OnStartup(e);

            string applicationDirectory = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) ?? ".";
            Directory.SetCurrentDirectory(applicationDirectory);

            _configService = (ConfigService) Container.Resolve<IConfigService>();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            _configService.SaveUser();

            base.OnExit(e);
        }

        protected override Window CreateShell() => Container.Resolve<MainWindow>();

        protected override IModuleCatalog CreateModuleCatalog() => new ConfigurationModuleCatalog();

        protected override void RegisterTypes(IContainerRegistry reg)
        {
            reg.RegisterSingleton<IBackgroundTaskService, BackgroundTaskService>();
            reg.RegisterSingleton<IConfigService, ConfigService>();
            reg.RegisterSingleton<IDialogService, DialogService>();
            reg.RegisterSingleton<IDocumentService, DocumentService>();

            reg.Register<JDocumentViewModel>();
            reg.Register<FileBrowserViewModel>();
            reg.Register<MainViewModel>();
        }

        private static Assembly OnAssemblyResolve(object sender, ResolveEventArgs args)
        {
            var domain = (AppDomain) sender;

            return domain.GetAssemblies().FirstOrDefault(a => a.FullName == args.Name);
        }
    }
}
