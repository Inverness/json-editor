﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Threading;

namespace JsonEditor.Infrastructure
{
    internal class BackgroundTaskService : DispatcherObject, IBackgroundTaskService
    {
        private readonly Progress<float?> _progress;

        private BackgroundTaskImpl _currentTask;
        private readonly Queue<BackgroundTaskImpl> _queue = new Queue<BackgroundTaskImpl>();

        public BackgroundTaskService()
        {
            _progress = new Progress<float?>(OnProgressReported);
        }

        public event EventHandler<BackgroundTaskEventArgs> TaskStarted;

        public event EventHandler<BackgroundTaskEventArgs> TaskProgress;

        public event EventHandler<BackgroundTaskEventArgs> TaskEnded;

        public async Task<T> Run<T>(string name, BackgroundFunc<T> func, CancellationToken ct = default(CancellationToken))
        {
            Func<object> internalFunc = () => func(_progress);

            return (T) await InternalRun(name, func, ct, internalFunc);
        }

        public Task Run(string name, BackgroundAction action, CancellationToken ct = default(CancellationToken))
        {
            Func<object> interanlFunc = () =>
            {
                action(_progress);
                return null;
            };

            return InternalRun(name, action, ct, interanlFunc);
        }

        private Task<object> InternalRun(string name, Delegate action, CancellationToken ct, Func<object> func)
        {
            if (name == null)
                name = "Working...";
            if (action == null)
                throw new ArgumentNullException(nameof(action));

            VerifyAccess();

            var tcs = new TaskCompletionSource<object>();
            var bgt = new BackgroundTaskImpl(name, action, tcs, ct, func);

            _queue.Enqueue(bgt);

            if (_currentTask == null)
                RunQueue();

            return tcs.Task;
        }

        private async void RunQueue()
        {
            while (_queue.Count != 0 && _currentTask == null)
            {
                _currentTask = _queue.Dequeue();

                try
                {
                    TaskStarted?.Invoke(this, new BackgroundTaskEventArgs(_currentTask, null));
                }
                catch
                {
                    _currentTask = null;
                    throw;
                }

                Exception exception = null;
                object result = null;
                try
                {
                    result = await Task.Factory.StartNew(_currentTask.InternalFunc,
                                                         _currentTask.CancellationToken,
                                                         TaskCreationOptions.None,
                                                         TaskScheduler.Default);
                }
                catch (Exception ex)
                {
                    exception = ex;
                }

                bool cancelled = _currentTask.CancellationToken.IsCancellationRequested;

                if (exception != null && !(exception is OperationCanceledException))
                     _currentTask.TaskCompletionSource.SetException(exception);
                else if (cancelled)
                    _currentTask.TaskCompletionSource.SetCanceled();
                else
                    _currentTask.TaskCompletionSource.SetResult(result);

                try
                {
                    TaskEnded?.Invoke(this, new BackgroundTaskEventArgs(_currentTask, null));
                }
                finally
                {
                    _currentTask = null;
                }
            }
        }

        private void OnProgressReported(float? f)
        {
            if (_currentTask == null)
                throw new InvalidOperationException("No current task");

            TaskProgress?.Invoke(this, new BackgroundTaskEventArgs(_currentTask, f));
        }
    }

    internal sealed class BackgroundTaskImpl : BackgroundTask
    {
        internal readonly Func<object> InternalFunc;
        internal readonly TaskCompletionSource<object> TaskCompletionSource;
        internal readonly CancellationToken CancellationToken;

        internal BackgroundTaskImpl(string name, Delegate action, TaskCompletionSource<object> tcs, CancellationToken ct, Func<object> internalFunc)
            : base(name, action)
        {
            TaskCompletionSource = tcs;
            CancellationToken = ct;
            InternalFunc = internalFunc;
        }
    }

    //internal sealed class BackgroundWorkerTaskScheduler : TaskScheduler
    //{
    //    [ThreadStatic]
    //    private static bool t_processing;
    //    private readonly object _instanceLock = new object();
    //    private readonly LinkedList<Task> _tasks = new LinkedList<Task>();
    //    private bool _running;

    //    protected override void QueueTask(Task task)
    //    {
    //        lock (_instanceLock)
    //        {
    //            _tasks.AddLast(task);
    //            if (!_running)
    //            {
    //                _running = true;
    //                ThreadPool.UnsafeQueueUserWorkItem(ThreadPoolRun, null);
    //            }
    //        }
    //    }

    //    protected override bool TryExecuteTaskInline(Task task, bool taskWasPreviouslyQueued)
    //    {
    //        if ((!t_processing) || (taskWasPreviouslyQueued && !_tasks.Remove(task)))
    //            return false;
    //        return TryExecuteTask(task);
    //    }

    //    protected override bool TryDequeue(Task task)
    //    {
    //        lock (_instanceLock)
    //            return _tasks.Remove(task);
    //    }

    //    protected override IEnumerable<Task> GetScheduledTasks()
    //    {
    //        lock (_instanceLock)
    //            return _tasks.ToArray();
    //    }

    //    private void ThreadPoolRun(object state)
    //    {
    //        t_processing = true;
    //        try
    //        {
    //            while (true)
    //            {
    //                Task task;
    //                lock (_tasks)
    //                {
    //                    if (_tasks.Count == 0)
    //                    {
    //                        _running = false;
    //                        break;
    //                    }

    //                    task = _tasks.First.Value;
    //                    _tasks.RemoveFirst();
    //                }

    //                TryExecuteTask(task);
    //            }
    //        }
    //        finally
    //        {
    //            t_processing = false;
    //        }
    //    }
    //}
}
