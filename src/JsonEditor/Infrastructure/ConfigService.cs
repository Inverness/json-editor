﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace JsonEditor.Infrastructure
{
    internal class ConfigService : IConfigService
    {
        private JObject _application;
        private JObject _user;

        public string ApplicationPath { get; } =
            Path.Combine(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location) ?? "", "Config.json");

        public string UserPath { get; } =
            Environment.ExpandEnvironmentVariables(@"%APPDATA%\JsonEditor\UserConfig.json");

        public JObject Application
        {
            get
            {
                if (_application == null)
                {
                    string path = ApplicationPath;
                    _application = File.Exists(path) ? JObject.Parse(File.ReadAllText(path)) : new JObject();
                }

                return _application;
            }
        }

        public JObject User
        {
            get
            {
                if (_user == null)
                {
                    string path = UserPath;

                    _user = File.Exists(path) ? JObject.Parse(File.ReadAllText(path)) : new JObject();
                }

                return _user;
            }
        }

        public void SaveUser()
        {
            JObject root = User;

            string path = UserPath;
            string parentDir = Path.GetDirectoryName(path);
            Debug.Assert(parentDir != null, "parentDir != null");
            if (!Directory.Exists(parentDir))
                Directory.CreateDirectory(parentDir);

            File.WriteAllText(path, root.ToString(Formatting.Indented));
        }
    }
}
