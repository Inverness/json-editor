﻿using System;
using System.Windows;
using JsonEditor.Views;
using Ookii.Dialogs.Wpf;

namespace JsonEditor.Infrastructure
{
    internal class DialogService : IDialogService
    {
        public string[] ShowOpenFileDialog(string filter, bool multiselect)
        {
            var dialog = new VistaOpenFileDialog
            {
                Filter = filter,
                Multiselect = multiselect
            };

            if (!dialog.ShowDialog().GetValueOrDefault())
                return CollectionUtility<string>.EmptyArray;

            return multiselect ? dialog.FileNames : new[] { dialog.FileName };
        }

        public string ShowFolderBrowserDialog()
        {
            var dialog = new VistaFolderBrowserDialog();
            if (!dialog.ShowDialog().GetValueOrDefault())
                return null;
            return dialog.SelectedPath;
        }

        public MessageBoxResult ShowMessageBox(string text, string caption,
                                               MessageBoxButton button = MessageBoxButton.OK,
                                               MessageBoxImage image = MessageBoxImage.None)
        {
            return MessageBox.Show(text, caption, button, image);
        }

        public string ShowInputDialog(string text, string caption)
        {
            var d = new InputDialog(text, caption);
            return d.ShowDialog().GetValueOrDefault() ? d.Value : null;
        }

        public void HandleError(Action action)
        {
            try
            {
                action();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().Name + ": " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }

        public T HandleError<T>(Func<T> action, T defaultValue = default(T))
        {
            try
            {
                return action();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.GetType().Name + ": " + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                return defaultValue;
            }
        }
    }
}
