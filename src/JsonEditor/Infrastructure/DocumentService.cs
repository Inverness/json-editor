﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using JsonEditor.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;

namespace JsonEditor.Infrastructure
{
    internal class DocumentService : IDocumentService
    {
        private const string SchemaFilePattern = "*.schema.json";
        private readonly JSchemaPreloadedResolver _resolver = new JSchemaPreloadedResolver();
        private readonly List<Tuple<Regex, Uri>> _patterns = new List<Tuple<Regex, Uri>>();
        
        private readonly IConfigService _configService;
        private readonly IBackgroundTaskService _backgroundTaskService;

        internal DocumentService(IConfigService cfg, IBackgroundTaskService bws)
        {
            _configService = cfg;
            _backgroundTaskService = bws;

            LoadSchemas();
            LoadSchemaPatterns();
        }

        public Task<JDocument> LoadDocumentAsync(string path)
        {
            return LoadDocumentAsync(path, (Uri) null);
        }

        public Task<JDocument> LoadDocumentAsync(string path, string schemaPath)
        {
            var schemaUri = schemaPath != null ? new Uri(Path.GetFullPath(schemaPath), UriKind.Absolute) : null;
            return LoadDocumentAsync(path, schemaUri);
        }

        /// <summary>
        /// Load a JSON document and schema.
        /// </summary>
        /// <param name="path">A path to a JSON file.</param>
        /// <param name="schemaUri">The URI of a JSON schema.</param>
        /// <returns>A task that completes when the document has been loaded.</returns>
        public async Task<JDocument> LoadDocumentAsync(string path, Uri schemaUri)
        {
            JObject root = null;
            JSchema schema = null;

            if (path != null)
            {
                path = Path.GetFullPath(path);

                BackgroundFunc<JObject> loadAction = p =>
                {
                    using (var reader = new JsonTextReader(File.OpenText(path)))
                        return JObject.Load(reader);
                };

                root = await _backgroundTaskService.Run("Loading document...", loadAction);
            }

            // Resolve the actual schema
            if (schemaUri != null)
            {
                schema = await ResolveBaseSchemaAsync(schemaUri);

                // Special file URI handling
                if (schema == null && schemaUri.Scheme == "file")
                {
                    // use a host of cd to represent the current directory.
                    string schemaPath = schemaUri.Host == "cd"
                                            ? schemaUri.AbsolutePath.Substring(1)
                                            : schemaUri.AbsolutePath;

                    BackgroundFunc<JSchema> action = p =>
                    {
                        using (var reader = new JsonTextReader(File.OpenText(schemaPath)))
                            return JSchema.Load(reader, _resolver);
                    };

                    schema = await _backgroundTaskService.Run("Loading schema file...", action);
                }

                if (schema == null)
                    throw new JSchemaException("unable to resolve schema: " + schemaUri);
            }

            // We now have enough information to begin editing.

            bool strictObjects = (bool?) schema?.GetExtensionData(SchemaExtensionNames.StrictObjects) ?? false;

            var document = new JDocument
            {
                StrictObjects = strictObjects,
                Root = root,
                Path = path,
                Schema = schema,
                SchemaUri = schemaUri,
                Modified = false
            };

            if (root != null && schema != null && strictObjects)
                document.GenerateStrictObjectProperties(true);

            // Root will be null if we excluded a path.
            if (root == null && schema != null)
                document.GenerateFromSchema();
            Debug.Assert(document.Root != null, "document.Root != null");

            return document;
        }

        /// <summary>
        /// Returns the URI for a schema that a pattern specifies for a path.
        /// </summary>
        public Uri GetSchemaUriForPath(string path)
        {
            return _patterns.Where(p => p.Item1.IsMatch(path)).Select(p => p.Item2).FirstOrDefault();
        }

        /// <summary>
        /// Resolves a top level schema with the specified URI
        /// </summary>
        private async Task<JSchema> ResolveBaseSchemaAsync(Uri uri)
        {
            var context = new ResolveSchemaContext();
            var sr = new SchemaReference {BaseUri = uri};

            Stream stream = _resolver.GetSchemaResource(context, sr);
            if (stream == null)
                return null;

            JSchema schema = null;

            BackgroundAction action = p =>
            {
                using (var reader = new JsonTextReader(new StreamReader(stream)))
                    schema = JSchema.Load(reader, _resolver);
            };

            await _backgroundTaskService.Run("Resolving schema...", action);

            return schema;
        }

        /// <summary>
        /// Pre-loads all schemas specified in schema directories.
        /// </summary>
        private void LoadSchemas()
        {
            foreach (string schemaDir in _configService.Application["SchemaDirectories"])
            {
                if (!Directory.Exists(schemaDir))
                    continue;

                foreach (string schemaFilename in Directory.EnumerateFiles(schemaDir,
                                                                           SchemaFilePattern,
                                                                           SearchOption.TopDirectoryOnly))
                {
                    using (var reader = new JsonTextReader(File.OpenText(schemaFilename)))
                    {
                        JSchema schema = JSchema.Load(reader, _resolver);
                        _resolver.Add(schema.Id, schema.ToString());
                    }
                }
            }
        }

        /// <summary>
        /// Loads schema patterns from config.
        /// </summary>
        private void LoadSchemaPatterns()
        {
            foreach (KeyValuePair<string, JToken> pattern in (JObject) _configService.Application["SchemaPatterns"])
            {
                var regex = new Regex(StringUtility.WildcardToRegex(pattern.Key));
                var schemaName = new Uri((string) pattern.Value);

                _patterns.Add(Tuple.Create(regex, schemaName));
            }
        }
    }
}