using System;
using System.Threading.Tasks;
using JsonEditor.Model;

namespace JsonEditor.Infrastructure
{
    /// <summary>
    /// Provides a way of loading documents and schemas from disk.
    /// </summary>
    internal interface IDocumentService
    {
        /// <summary>
        /// Load a JSON document and schema. The schema will be loaded by matching the specified path to schema patterns.
        /// </summary>
        /// <param name="path">A path to a JSON file.</param>
        /// <returns>The loaded document.</returns>
        Task<JDocument> LoadDocumentAsync(string path);

        /// <summary>
        /// Load a JSON document and schema.
        /// </summary>
        /// <param name="path">A path to a JSON file.</param>
        /// <param name="schemaPath">A path to a schema file.</param>
        /// <returns>The loaded document.</returns>
        Task<JDocument> LoadDocumentAsync(string path, string schemaPath);

        /// <summary>
        /// Load a JSON document and schema.
        /// </summary>
        /// <param name="path">A path to a JSON file.</param>
        /// <param name="schemaUri">The URI of a JSON schema.</param>
        /// <returns>The loaded document.</returns>
        Task<JDocument> LoadDocumentAsync(string path, Uri schemaUri);

        /// <summary>
        /// Get the URI of a schema that is valid for a document with the specified path.
        /// </summary>
        /// <param name="path">A path to a JSON file.</param>
        /// <returns>A matching schema URI, or null.</returns>
        Uri GetSchemaUriForPath(string path);
    }
}