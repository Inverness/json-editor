﻿using System.Collections.ObjectModel;
using System.IO;
using System.Windows.Input;
using System.Windows.Media;
using JsonEditor.Infrastructure;
using Prism.Commands;
using Prism.Mvvm;

namespace JsonEditor.ViewModels
{
    internal class FileBrowserViewModel : AnchorableViewModel
    {
        private const string FileBrowserPathKey = "FileBrowserPath";

        private readonly IDialogService _dialogService;
        private readonly IConfigService _configService;
        private string _selectedPath;

        public FileBrowserViewModel(IDialogService dialogService, IConfigService configService)
        {
            _dialogService = dialogService;
            _configService = configService;

            Title = "File Browser";
            PathItems = new ObservableCollection<FileBrowserItem>();
            SelectPathCommand = new DelegateCommand(ExecuteSelectDirectory);
            ExecuteItemCommand = new DelegateCommand<FileBrowserItem>(ExecuteItem);

            SelectedPath = (string) configService.User[FileBrowserPathKey];
        }

        public string SelectedPath
        {
            get { return _selectedPath; }

            set
            {
                if (SetProperty(ref _selectedPath, value))
                    OnSelectedPathChanged();
            }
        }

        public ObservableCollection<FileBrowserItem> PathItems { get; } 

        public ICommand SelectPathCommand { get; private set; }

        public ICommand ExecuteItemCommand { get; }

        private void ExecuteSelectDirectory()
        {
            string path = _dialogService.ShowFolderBrowserDialog();
            if (path == null)
                return;

            SelectedPath = path;
        }

        private void OnSelectedPathChanged()
        {
            _configService.User[FileBrowserPathKey] = SelectedPath;
            _dialogService.HandleError(UpdatePathItems);
        }

        private void UpdatePathItems()
        {
            PathItems.Clear();

            if (!Directory.Exists(SelectedPath))
                return;

            PathItems.Add(new FileBrowserItem("..", Resources.FolderIcon16, ExecuteItemCommand));

            foreach (string path in Directory.EnumerateDirectories(SelectedPath, "*", SearchOption.TopDirectoryOnly))
            {
                string fileName = Path.GetFileName(path);

                PathItems.Add(new FileBrowserItem(fileName, Resources.FolderIcon16, ExecuteItemCommand));
            }

            foreach (string path in Directory.EnumerateFiles(SelectedPath, "*", SearchOption.TopDirectoryOnly))
            {
                string fileName = Path.GetFileName(path);

                PathItems.Add(new FileBrowserItem(fileName, null, ExecuteItemCommand));
            }
        }

        private void ExecuteItem(FileBrowserItem item)
        {
            if (item == null)
                return;

            if (item.Name == "..")
            {
                string parent = Path.GetDirectoryName(SelectedPath);
                if (parent != null)
                    SelectedPath = parent;
            }
            else
            {
                string maybePath = Path.Combine(SelectedPath, item.Name);
                if (Directory.Exists(maybePath))
                    SelectedPath = maybePath;
            }
        }
    }

    internal class FileBrowserItem : BindableBase
    {
        private readonly ICommand _baseCommand;

        internal FileBrowserItem(string name, ImageSource image, ICommand baseCommand)
        {
            _baseCommand = baseCommand;
            Name = name;
            Image = image;
            Command = new DelegateCommand(Execute, CanExecute);
        }

        public string Name { get; set; }

        public ImageSource Image { get; set; }

        public ICommand Command { get; set; }

        private bool CanExecute()
        {
            return _baseCommand != null;
        }

        private void Execute()
        {
            _baseCommand.Execute(this);
        }
    }
}
