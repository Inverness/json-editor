﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using JsonEditor.Infrastructure;
using JsonEditor.Model;
using JsonEditor.Views;
using Prism.Commands;
using Prism.Mvvm;
using Unity;

namespace JsonEditor.ViewModels
{
    internal class MainViewModel : BindableBase
    {
        private readonly IUnityContainer _compositionContainer;
        private readonly IDialogService _dialogService;
        private readonly IBackgroundTaskService _backgroundTaskService;
        private readonly IDocumentService _documentService;
        private JDocumentViewModel _selectedDocument;

        private string _statusText;
        private bool _progressVisible;
        private double _progressValue;
        private bool _progressIndeterminate;

        internal MainViewModel(
            IUnityContainer cc,
            IDialogService ds,
            IBackgroundTaskService bws,
            IDocumentService docs,
            FileBrowserViewModel fbvm)
        {
            _compositionContainer = cc;
            _dialogService = ds;
            _backgroundTaskService = bws;
            _backgroundTaskService.TaskStarted += OnBackgroundTaskStarted;
            _backgroundTaskService.TaskEnded += OnBackgroundTaskEnded;
            _backgroundTaskService.TaskProgress += OnBackgroundTaskProgress;
            _documentService = docs;
            
            Tools = new ObservableCollection<AnchorableViewModel> { fbvm };
            Documents = new ObservableCollection<JDocumentViewModel>();
            OpenDocumentCommand = new DelegateCommand(ExecuteOpenDocument);
            OpenDocumentWithSchemaCommand = new DelegateCommand(ExecuteOpenDocumentWithSchema);

            Test();
        }

        private async void Test()
        {
            AddViewModel(await _documentService.LoadDocumentAsync("TestData/DemoArchetypes3.rdec",
                                                                  "Schemas/ActorArchetypeFile.schema.json"));
        }

        /// <summary>
        /// Gets or sets the currently selected document tab.
        /// </summary>
        public JDocumentViewModel SelectedDocument
        {
            get { return _selectedDocument; }

            set { SetProperty(ref _selectedDocument, value); }
        }

        /// <summary>
        /// Gets a collection containing the currently open tool windows.
        /// </summary>
        public ObservableCollection<AnchorableViewModel> Tools { get; private set; }

        /// <summary>
        /// Gets a collection containing the currently open documents.
        /// </summary>
        public ObservableCollection<JDocumentViewModel> Documents { get; }

        /// <summary>
        /// Gets a command that opens a document without loading a schema.
        /// </summary>
        public DelegateCommand OpenDocumentCommand { get; private set; }

        /// <summary>
        /// Gets a command that opens a document along with a schema.
        /// </summary>
        public DelegateCommand OpenDocumentWithSchemaCommand { get; private set; }

        /// <summary>
        /// Gets or sets the current status text.
        /// </summary>
        public string StatusText
        {
            get { return _statusText; }

            set { SetProperty(ref _statusText, value); }
        }

        /// <summary>
        /// Gets or sets whether the progress bar is visible.
        /// </summary>
        public bool ProgressVisible
        {
            get { return _progressVisible; }

            set { SetProperty(ref _progressVisible, value); }
        }

        /// <summary>
        /// Gets or sets the current progress value.
        /// </summary>
        public double ProgressValue
        {
            get { return _progressValue; }

            set { SetProperty(ref _progressValue, value); }
        }

        /// <summary>
        /// Gets or sets whether the progress bar is in indeterminate mode.
        /// </summary>
        public bool ProgressIndeterminate
        {
            get { return _progressIndeterminate; }

            set { SetProperty(ref _progressIndeterminate, value); }
        }
      

        private async void ExecuteOpenDocument()
        {
            string jsonFilename = _dialogService.ShowOpenFileDialog(null, false).FirstOrDefault();
            if (jsonFilename != null)
                AddViewModel(await _documentService.LoadDocumentAsync(jsonFilename));
        }

        private async void ExecuteOpenDocumentWithSchema()
        {
            var dialog = new OpenDocumentDialog();
            if (!dialog.ShowDialog().GetValueOrDefault())
                return;

            string docpath = dialog.DocumentPath;
            string schemapath = dialog.SchemaPath;

            if (string.IsNullOrWhiteSpace(docpath))
                return;

            JDocument doc;

            if (!string.IsNullOrWhiteSpace(schemapath))
            {
                doc = await _documentService.LoadDocumentAsync(docpath, schemapath);
            }
            else
            {
                Uri schemaUri = _documentService.GetSchemaUriForPath(docpath);

                doc = await _documentService.LoadDocumentAsync(docpath, schemaUri);
            }

            AddViewModel(doc);
        }

        private void OnBackgroundTaskStarted(object sender, BackgroundTaskEventArgs e)
        {
            StatusText = e.Task.Name;
            ProgressVisible = true;

            if (e.Progress.HasValue)
            {
                ProgressIndeterminate = false;
                ProgressValue = e.Progress.Value;
            }
            else
            {
                ProgressIndeterminate = true;
            }
        }

        private void OnBackgroundTaskEnded(object sender, BackgroundTaskEventArgs e)
        {
            StatusText = null;
            ProgressVisible = false;
        }

        private void OnBackgroundTaskProgress(object sender, BackgroundTaskEventArgs e)
        {
            if (e.Progress.HasValue)
            {
                ProgressIndeterminate = false;
                ProgressValue = e.Progress.Value;
            }
            else
            {
                ProgressIndeterminate = true;
            }
        }

        private void AddViewModel(JDocument doc)
        {
            //var vm = new JDocumentViewModel(doc,
            //                                _dialogService,
            //                                _compositionContainer.GetExportedValues<IEditorFactory>());
            //var vm = _compositionContainer.GetExportedValue<JDocumentViewModel>();
            var vm = _compositionContainer.Resolve<JDocumentViewModel>();
            vm.Initialize(doc);

            Documents.Add(vm);

            SelectedDocument = vm;
        }
    }
}