﻿using System.Windows;
using System.Windows.Input;

namespace JsonEditor.Views
{
    public static class ExtendedCommands
    {
        public static readonly DependencyProperty DoubleClickCommandProperty =
            DependencyProperty.RegisterAttached("DoubleClickCommand", typeof (ICommand), typeof (ExtendedCommands),
                                                new UIPropertyMetadata(null, OnDoubleClickCommandPropertyChanged));

        public static readonly DependencyProperty DoubleClickCommandParameterProperty =
            DependencyProperty.RegisterAttached("DoubleClickCommandParameter", typeof (object),
                                                typeof (ExtendedCommands), new UIPropertyMetadata(null));

        public static ICommand GetDoubleClickCommand(DependencyObject obj)
        {
            return (ICommand) obj.GetValue(DoubleClickCommandProperty);
        }

        public static void SetDoubleClickCommand(DependencyObject obj, ICommand value)
        {
            obj.SetValue(DoubleClickCommandProperty, value);
        }

        public static object GetDoubleClickCommandParameter(DependencyObject obj)
        {
            return obj.GetValue(DoubleClickCommandParameterProperty);
        }

        public static void SetDoubleClickCommandParameter(DependencyObject obj, object value)
        {
            obj.SetValue(DoubleClickCommandParameterProperty, value);
        }

        private static void OnDoubleClickCommandPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            var element = d as UIElement;
            if (element != null)
            {
                if (e.OldValue == null && e.NewValue != null)
                {
                    element.MouseDown += OnMouseDown;
                }
                else if (e.OldValue != null && e.NewValue == null)
                {
                    element.MouseDown -= OnMouseDown;
                }
            }
        }

        private static void OnMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount != 2)
                return;

            var element = sender as UIElement;
            if (element == null)
                return;

            ICommand command = GetDoubleClickCommand(element);
            object parameter = GetDoubleClickCommandParameter(element);

            if (command != null && command.CanExecute(parameter))
            {
                command.Execute(parameter);
                e.Handled = true;
            }
        }
    }
}
