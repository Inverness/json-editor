﻿using System.Windows;

namespace JsonEditor.Views
{
    /// <summary>
    /// Interaction logic for InputDialog.xaml
    /// </summary>
    public partial class InputDialog : Window
    {
        public InputDialog()
        {
            InitializeComponent();
            SourceInitialized += (sender, args) => IconUtility.RemoveIcon((Window) sender);

            InputTextBox.Focus();
        }

        public InputDialog(string text, string title)
        {
            InitializeComponent();
            SourceInitialized += (sender, args) => IconUtility.RemoveIcon((Window) sender);

            Text = text;
            Title = title;

            InputTextBox.Focus();
        }

        public string Text
        {
            get { return TextBlock.Text; }

            set { TextBlock.Text = value; }
        }

        public string Value
        {
            get { return InputTextBox.Text; }

            set { InputTextBox.Text = value; }
        }

        private void OnCancelClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        private void OnOkClicked(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
