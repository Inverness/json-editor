﻿using System.Windows;
using System.Windows.Controls;
using JsonEditor.ViewModels;

namespace JsonEditor.Views
{
    internal class LayoutItemContainerStyleSelector : StyleSelector
    {
        public Style DocumentStyle { get; set; }

        public Style AnchorableStyle { get; set; }

        public override Style SelectStyle(object item, DependencyObject container)
        {
            if (item is DocumentViewModel)
                return DocumentStyle;

            if (item is AnchorableViewModel)
                return AnchorableStyle;

            return base.SelectStyle(item, container);
        }
    }
}
