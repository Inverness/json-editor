﻿using System.Windows;
using System.Windows.Controls;
using JsonEditor.ViewModels;

namespace JsonEditor.Views
{
    internal class LayoutItemTemplateSelector : DataTemplateSelector
    {
        public DataTemplate JDocumentTemplate { get; set; }

        public DataTemplate FileBrowserTemplate { get; set; }

        public override DataTemplate SelectTemplate(object item, DependencyObject container)
        {
            if (item is JDocumentViewModel)
                return JDocumentTemplate;

            if (item is FileBrowserViewModel)
                return FileBrowserTemplate;

            return base.SelectTemplate(item, container);
        }
    }
}
