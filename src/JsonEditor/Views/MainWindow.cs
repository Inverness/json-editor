﻿using System.Windows;
using JsonEditor.ViewModels;
using Unity;

namespace JsonEditor.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        internal MainWindow()
        {
            InitializeComponent();
        }

        [Dependency]
        internal MainViewModel ViewModel
        {
            get { return DataContext as MainViewModel; }

            set { DataContext = value; }
        }
    }
}
