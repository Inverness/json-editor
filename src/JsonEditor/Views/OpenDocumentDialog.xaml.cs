﻿using System.Windows;
using System.Windows.Controls;
using Ookii.Dialogs.Wpf;

namespace JsonEditor.Views
{
    /// <summary>
    /// Interaction logic for OpenDocumentWindow.xaml
    /// </summary>
    public partial class OpenDocumentDialog : Window
    {
        public OpenDocumentDialog()
        {
            InitializeComponent();
            SourceInitialized += (sender, e) => IconUtility.RemoveIcon((Window) sender);
            DocumentPathTextBox.Focus();
        }

        public string DocumentPath
        {
            get { return DocumentPathTextBox.Text; }

            set { DocumentPathTextBox.Text = value; }
        }

        public string SchemaPath
        {
            get { return SchemaPathTextBox.Text; }

            set { SchemaPathTextBox.Text = value; }
        }

        private void DocumentBrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            string path = ShowFileSelectionDialog(null);
            if (path != null)
                DocumentPathTextBox.Text = path;
        }

        private void SchemaBrowseButton_OnClick(object sender, RoutedEventArgs e)
        {
            const string schemaFilter = "JSON Schema Files (*.schema.json)|*.schema.json|All files (*.*)|*.*";

            string path = ShowFileSelectionDialog(schemaFilter);
            if (path != null)
                SchemaPathTextBox.Text = path;
        }

        private string ShowFileSelectionDialog(string filter)
        {
            var dialog = new VistaOpenFileDialog
            {
                Filter = filter,
                Multiselect = false
            };

            if (!dialog.ShowDialog().GetValueOrDefault())
                return null;

            return dialog.FileName;
        }

        private void OpenButton_OnClick(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        private void DocumentPathTextBox_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            // TODO: Show matching schema URI
        }
    }
}
